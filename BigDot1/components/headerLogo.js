/* eslint-disable prettier/prettier */
import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Icon1 from 'react-native-vector-icons/Ionicons';

const HeaderLogo = (props) => {
    const Logo = require('../assets/Deloittewhite.png');
  return (
    <View style={styles.header}>
        <Image resizeMode= "contain"  source={Logo} style={styles.Logo} />
        
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
   
  },
  Logo: {
    width: 130,
    height: 40,
    marginTop: 15,
    marginLeft: 10,
  },
  container: {
    flexDirection: 'row',
  },
});

export default HeaderLogo;
