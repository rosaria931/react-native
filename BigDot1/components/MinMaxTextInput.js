/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Image} from 'react-native';

export default class MinMaxTextInput extends Component {
  static defaultProps = {
    minLength: 0,
    maxLength: 999,
  };
  getCharacter = numCharacters => {
    return numCharacters === 1 ? 'character' : 'characters';
  };

  // x character needed
  getCharacterMessage = numCharacters => {
    return `${numCharacters} ${this.getCharacter(numCharacters)} needed`;
  };
  // x more characters needed
  getMoreCharacterMessage = numCharacters => {
    return `${numCharacters} more ${this.getCharacter(numCharacters)} needed`;
  };
  // x characters remaining
  getCharacterRemainingMessage = numCharacters => {
    return `${numCharacters} ${this.getCharacter(numCharacters)} remaining`;
  };

  renderMessage() {
    var len = this.props.value ? this.props.value.length : 0;
    var msg = '';

    // User has max # of characters...
    if (len === this.props.maxLength) {
      return <View />;
    }
    // User has more than min # of characters...
    if (len >= this.props.minLength) {
      msg = this.getCharacterRemainingMessage(this.props.maxLength - len);
      return <Text style={styles.neutralText}>{msg}</Text>;
    }
    // User does not have enough characters, but hasn't started typing yet....
    if (len < this.props.minLength && len === 0) {
      msg = this.getCharacterMessage(this.props.minLength);
      return <Text style={styles.neutralText}>{msg}</Text>;
    }
    // User does not have enough characters, and has started typing....
    if (len < this.props.minLength && len > 0) {
      msg = this.getMoreCharacterMessage(this.props.minLength - len);
      return <Text style={styles.invalidText}>{msg}</Text>;
    }
    return <View />;
  }

  render() {
    return (
      <View>
        <TextInput {...this.props} multiline={true} editable={true}/>
        {this.renderMessage()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  invalidText: {
    color: 'orange',
  },
  neutralText: {
    paddingTop: 5,
  },
});
