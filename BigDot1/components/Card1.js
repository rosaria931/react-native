import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default function Card1(props) {
  return (
    <View styles={styles.card}>
      <View styles={styles.cardContent}>{props.children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    width: 350,
    height: '90%',
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowRadius: 2,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 12,
    borderRadius: 8,
  },
  cardContent: {
    marginHorizontal: 18,
    marginVertical: 10,
  },
  cardImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  cardText: {
    padding: 10,
    fontSize: 16,
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
