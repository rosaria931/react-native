/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  RefreshControl,
  FlatList,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Card1 from '../components/Card1';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
export default class PendingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      dataSource: [],
      color1: 'white',
      color2: 'green',
      dataArray: [],
    };
  }
  _onRefresh() {
    this.setState({loading: true});
    this.fetchData();
  }
  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    this.setState({loading: true});
    fetch('https://9e564052c901.ngrok.io/')
      .then(response => response.json())
      .then(responseJson => {
        responseJson1 = responseJson.filter(function(e) {
          return e.status === 5 && e.body === null;
        });
        responseJson = responseJson1.map(item => {
          AsyncStorage.getItem('card').then(array => {
            if (array !== null) {
              if (array.includes(item._id) === true) {
                item.isSelect1 = !item.isSelect1;
                item.selectedClass = item.isSelect1;
              } else {
              }
            }
          });

          return item;
        });

        this.setState({
          loading: false,
          dataSource: responseJson,
        });

      })

      .catch(error => {
        this.setState({loading: false});
      });
  };


  selectItem = data => {
    console.log('select item');
    data.item.isSelect1 = !data.item.isSelect1;
    data.item.selectedClass = data.item.isSelect1
      ? styles.selected
      : styles.list;

    const index = this.state.dataSource.findIndex(
      item => data.item._id === item._id,
    );

    this.state.dataSource[index] = data.item;

    this.setState({
      dataSource: this.state.dataSource,
    });

    AsyncStorage.getItem('card')
      .then(array => {
        if (array !== null) {
          const card = JSON.parse(array);

          if (array.includes(data.item._id) !== true) {
            card.push(data);
            AsyncStorage.setItem('card', JSON.stringify(card));

            alert('Added to Favorites');
          } else {
            const postsItems = card.filter(function(e) {
              return e.item._id !== data.item._id;
            });

            AsyncStorage.setItem('card', JSON.stringify(postsItems));
            alert('Removed from Favorites');
          }
        } else {
          AsyncStorage.clear();
          const card = [];
          card.push(data);
          AsyncStorage.setItem('card', JSON.stringify(card));

          alert('Added to Favourites');
        }
      })
      .catch(err => {
        alert(err);
      });
  };

  selectItem1 = data => {
    data.item.isSelect = !data.item.isSelect;
    data.item.selectedClass1 = data.item.isSelect
      ? styles.selected
      : styles.list;

    const index = this.state.dataSource.findIndex(
      item => data.item._id === item._id,
    );

    this.state.dataSource[index] = data.item;

    this.setState({
      dataSource: this.state.dataSource,
    });

    AsyncStorage.getItem('like')
      .then(array1 => {
        if (array1 !== null) {
          const card1 = JSON.parse(array1);

          if (array1.includes(data.item._id) !== true) {
            card1.push(data);
            AsyncStorage.setItem('like', JSON.stringify(card1));

            alert('I like it!');
          } else {
            const postsItems1 = card1.filter(function(e) {
              return e.item._id !== data.item._id;
            });

            AsyncStorage.setItem('like', JSON.stringify(postsItems1));
            alert('Removed like :(');
          }
        } else {
          AsyncStorage.clear();
          const card1 = [];
          card1.push(data);
          AsyncStorage.setItem('like', JSON.stringify(card1));

          alert('I like it!');
        }
      })
      .catch(err => {
        alert(err);
      });
  };

  renderItem = data => (
    <View style={{flex: 1}}>
      <TouchableOpacity
        transparent
        onPress={() =>
          this.props.navigation.navigate('ComunicationScreen2', {
            id: data.item._id,
            title: data.item.title,
            data: data.item.date,
            img: data.item.imgUrl,
            pdf: data.item.pdfUrl,
            communication: data.item,
          })
        }>
        {/* itemId: this.state.dataSource._id}*/}
        <Card1>
          <View style={styles.card}>
            <Image style={styles.image} source={{uri: data.item.imgUrl}} />
            <View style={styles.view}>
              <View style={styles.textView}>
                <Text style={styles.dateText}>{data.item.date}</Text>
                <Text style={styles.text}>{data.item.title}</Text>
              </View>
              <View style={styles.iconsView}>
                <TouchableOpacity onPress={() => this.selectItem(data)}>
                  <Icon2
                    style={[styles.list, data.item.selectedClass]}
                    name="md-archive"
                    size={23}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.selectItem1(data)}>
                  <Icon
                    style={[styles.list, data.item.selectedClass1]}
                    name="heart"
                    size={22}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Card1>
      </TouchableOpacity>
    </View>
  );

  renderItem1 = data => (
    <TouchableOpacity
      style={[styles.list, data.item.selectedClass]}
      onPress={() =>
        this.props.navigation.navigate('ComunicationScreen2', {
          id: data.item._id,
          title: data.item.title,
          data: data.item.date,
          img: data.item.imgUrl,
          pdf: data.item.pdfUrl,
          communication: data.item,
        })
      }>
      <ImageBackground
        style={styles.imageA}
        imageStyle={styles.imageA}
        source={{uri: data.item.imgUrl}}>
        <Text style={styles.textA}>{data.item.title}</Text>
        <Text style={styles.textB}>{data.item.date}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );


  render() {
    const itemNumber = this.state.dataSource.filter(item => item.isSelect)
      .length;
    if (this.state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="grey" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={{height: (height - 170) / 2}}>
          <FlatList
            horizontal
            data={this.state.dataSource}
            renderItem={item => this.renderItem1(item)}
            keyExtractor={item => item._id.toString()}
            ListFooterComponent={this.renderFooter}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh.bind(this)}
                tintColor="grey"
                title="loading..."
              />
            }
          />
        </View>
        <View style={{height: (height - 85) / 1.85}}>
          <FlatList
            data={this.state.dataSource}
            renderItem={item => this.renderItem(item)}
            keyExtractor={item => item._id.toString()}
            extraData={this.state}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh.bind(this)}
                tintColor="grey"
                title="loading..."
              />
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 4,
    backgroundColor: 'black',
  },
  imagecomunication: {
    height: height,
    width: width,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 270,
  },
  view: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    flex: 0,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',
    position: 'relative',
    resizeMode: 'contain',
    shadowColor: 'black',
    shadowRadius: 4,
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    justifyContent: 'flex-start',
    alignItems: 'center',
    elevation: 40,
    borderRadius: 0,
  },
  image: {
    width: width / 2.7,
    height: height / 7,
    borderRadius: 15,

    marginLeft: 10,
    position: 'relative',
    opacity: 0.7,
  },
  textView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    fontFamily: 'verdana',
    width: width / 2,

    alignItems: 'stretch',
    height: height / 15,
    position: 'relative',
  },
  dateText: {
    fontSize: 11,
    fontFamily: 'verdana',
    color: 'grey',
    marginLeft: 10,
  },
  text: {
    fontSize: 14,
    color: 'white',
    height: height / 18,
    fontFamily: 'verdana',
    marginLeft: 10,
    width: width / 2,
    fontWeight: 'bold',
  },
  icons: {
    marginHorizontal: 7,
  },
  iconsView: {
    flexDirection: 'row',
    height: height / 30,
    position: 'relative',

    justifyContent: 'flex-end',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    width: width / 1.7,
    marginTop: 7,
  },
  textInput: {
    fontFamily: 'verdana',
    marginRight: 10,
    borderRadius: 30,
    fontSize: 20,
    color: 'green',
  },
  viewSearch: {},
  icons1: {
    marginHorizontal: 7,
  },
  icons2: {
    marginHorizontal: 7,
  },
  imageA: {
    height: windowHeight / 2.5,
    width: windowWidth,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.7,
  },
  textA: {
    fontSize: 15,
    color: 'white',
    marginTop: 150,
    fontWeight: 'bold',
    fontStyle: 'normal',
    position: 'relative',
    fontFamily: 'verdana',
  },
  textB: {
    fontSize: 11,
    color: 'white',
    marginTop: 2,
    textAlign: 'center',
    position: 'relative',
    fontFamily: 'verdana',
    fontWeight: 'bold',
  },
  list: {
    paddingVertical: 0,
    margin: 0,
    flexDirection: 'row',
    backgroundColor: 'black',
    justifyContent: 'flex-start',
    alignItems: 'center',
    zIndex: -1,
    color: 'white',
    marginHorizontal: 7,
  },
  selected: {
    color: 'green',
  },
  lightText: {
    color: '#f7f7f7',
    width: 200,
    paddingLeft: 15,
    fontSize: 12,
  },
});
