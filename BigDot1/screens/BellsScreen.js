/* eslint-disable prettier/prettier */
import React, {useState, useEffect} from 'react';
import {Button, View, Text, StyleSheet, Dimensions, TextInput, Image,TouchableOpacity, FlatList, Platform} from 'react-native';

import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
Icon.loadFont();
Icon1.loadFont();


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function SaveScreen ({navigation})  {
  const [review, setReview] = useState([
    {
      title: 'Comunication1',
      date: 'Experience Analytics Bootcamp 2020  ',
      body1: 'Sono aperte le iscrizioni. Non dimenticare di metterti in lista!',
      body2: '',
      image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEhASERIVEBIVERAQEBYXFRAQEhUQFRUWFhUVGBUYHSggGBoxGxUWITEhJSkrMC4uFx8zODMsNyotLysBCgoKDg0OGBAQGi8mICUtLS0rLSsvLS0tLS0uKy0tKzAtLS0tLS0tKy0tLS0rLS0tLS0rKy0tLS0rLS0tLS0rLf/AABEIAHABwgMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQUCAwQGB//EADgQAAIBAgMFBQUIAQUAAAAAAAABAgMRBCExBRJBUWEGEyJxgTJCkbHBFCNSYoKSodHhFSQz8PH/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAQIDBAX/xAAmEQEAAgIBBAEDBQAAAAAAAAAAAQIDESEEEjFRQSJxgQUTYZHx/9oADAMBAAIRAxEAPwD4aAAAAAAAAAAAAAAAAAABMSCYhMMgASkAAEpGyEBCJd7B2cpydSovuqdpT4bz92Hr8rmWTJFY3KlraXGFp0sLg6UqztKtLv8Ad1k42tTVuW7d3/MU2K7RKTyp3S9m8rJfpS+pr7TYyVWe/J3bl5JK2SS4K3ApDHDgiY77+ZRFInmVu9uc6Uf3SRrntSL9xr9Sf0KwG8Yqx8LdkOyeN5JmieIb6GoForELagbMoSaaa1WaMQWFj3qkr/HzIucEZNaGz7Q+hn2ele1YYerKLvF2+q5M7qlJVIuUVmleS6c10+RRfaZdDOlj6sWpRlutaNJGdsUzzDO2OfMMMVTszSX+Jq0sRRc9xQrRa7xRvFO/vqOi8lzKKpCzsaY77jU+Vsd98T5YgA0aAAAxZBLIIVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYkExCYZAAlISiDKGq8yCXZhqLk0lq2kvM9BiqqhTjSj7Mc3+ab1b/AIXkjg2fT3by46L6/wDepNa7ZyW+qznjmdqzaM8lxzv6WOG5vxtS8stFl68TTSpuTtFNvkszqrGobQi5MbuySu3kvM66eBmpJSyvbOzkknxyvnwsX2B2ZRqTi6ctxWspb1mpr3220k09F8yl8taRuUTbTzKozak1CTUc5OztFXtnyzyNdz7Vsbbc4ypYOtGdV7sk68m5QqTWaV0mtHa7eq6m+t2dwU1OP2WmpTd5WjnfOzT4ehw2/UorOrV+2lP3Xw64uen7UdkKmHbnSvUo5vR78Okl9Ty5348tcle6stItvwm4uYgunbK4uYgG3Zs6ruzu9NH1TN22aChNJZpwU4vnGV7HFSL7b1JSo0Jr2qcI0pdYttp/Fv4mNp7ckfyxmdZIn28+ADduAADFkEsghUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJiQTEJhkACUgTBnRpOcoxiruTUV5sieIJeppULrVRhGKlOTyjFcW/jbm+BS7S2lF3hSuo6OTylL091dNS07Z4jdn9mpvwU2u9f466VnfotEvN8Ty7OfBTcRaWOOPpiUEpkA6Gi72PilKdJVLyUJNx8N83xk114l5VwlHfdR3Slbeinlf8AF5nkMNi6kL7knHnoWWGx85brm99rhZK1+OXQ5M2K0zusqTD6ls/acVGEIq7ilCcb3ko2upLmmuP1uW0sTem1F3elmmstVZ52fXM+Z7L2m7p2s14VLlHk/gXkdrScbRd7+LJt29F5XseLl6aYsxmr0GP2gmkpe04q6dt5X1vbU+V9rNlRoVE4exO7S4KS1S6F1/qM51FHq1q7tp83od22aKq0JRUd+TV45q6qPRZ6efU6umi3T3j1Plau6y+dEG7E4adOW7OLhLk00zSe35bgAA2U2eoow7ym0s1KLXr/AOnlYstdmY6pSd6crc00pRfmnkY5qTMbhllruOFY0QXW3sDBPvaV92aVRxfuuSTlbpdspTSl4tG4bVtFo2AAuliyCWQQqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAExIJiEwyABKQvuydFd7Cb4SVvR3fyKEvtlVN1w9DHPvsmIZ5Z+lXY2bk3J5ttyfVvNs4JI7qyOSqi1OIRTw1m6FOL0bT5amklF13fRoRyWrXif0RrqV3FtW43zdzrhVis8k5Z8fJHPtBRbumtWna2hlEzNtSh0YOs7xTecmrapW8uJ6XApQ7xSbteN3HLezzW9y6HkKVRyi07pJ3y5cuqPT7KVaG/ONSE1KW93bW/T0sr2etrf5OfqaR272paEVtiSd5J3im0tW2THZLhGMlVs3mr3s+jsWlTtdThaNei4yS3ko7s4S5NN2/k6cVRlUo7273Ms5WeaV81G642epxTky1138R+FNz8uKnUpbnd1V3t/ckozUesb6eeRX7Q7M4aSbo1HSl+Cdrfpd8/IraWNspJX7xvN3VrequYvFuKUp7zVr73PpHpwOiuLJWfplaIlSYijKEpQlk07M1G7FV3OUpy1buaT0o3rlqHXgZZ7r9P6OQ34OjKc4xgrybSj5kW1qdomNw9FtWG7hqcn70VCHXm/h8zzJe9r5SVZUm7xowhRjbS+6nN/ub+CKIy6euqb98owxqoADdoxZBLIIVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYkExCYZAAlIWeHnoysN+GxG6881/PoUvXcK3jcOnFLxPrn8czirlvWo78FUp+OPsytrF6reXDj0yKnF+1blZFMc74Z0n4aDKFuJiDVo641ovdTySy624G+VKnDOXibzXX0OGnbr/B20Zvwq6Svm7Jyt5sztCJaO/k5RdrK6suBa4hSi9+nJrnb+tH5HE3BOW+3PK0bNZZ630v5GzC47ckpy9m1rJJve9St4mfH+ol6bYtCliKSliIqU4Tdndx8OTTty8z09Wq5KztZpr+sj51Pact6U4PwTioVY8o6X89TVDadam91VZ7ilFrxNq101rwaPPy9HfJO9/aPTOaTL02L7OwdRuKavnlpfoY7Y2YnRVNu0+GnmnkcWI29W34ptRheLhJZytZ73pn6HVUqOaU3NRTXFN5dbeTKxXNWazaTl4ZkG3E0XCTi+HLRrVNGo9hsk9b2SwShKNSSzyflE87szD7889Fm/oj1VauqVGcnldbq5tvL5XOTqrTMdkfLLJPGoee2zX36lWb9+c5fubZWmdao5O5gdNK9tYhrSuq6AAXWYsglkEKgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABMSCYhMMgASkAAHbsjGOnUT91+Ga/K+PmtTs2pRj3kslnZp9GkymRZQq78F+OKtbi4fW3yMb11buY3rq0WcdTDcjnaLC5qr07q/FfIvE+14lyxNvfcNf4NYLaW0l2NqqRcbSvqrZI0gaNOi6hnGal+n6vQ11sRvWySsrehrA0aFN3T5aG5Y6puuCm1Fu7jwuaQJiJNIlJvUg3U6TfRHZRpJaIrNtKzOlx2Z2Y3FN5bzv1tosjh7UYyM6vdw/46XhXWfvP6ehf4/HRw2EppP7+tTXdr8FO3iqPrql6vgeGOXp4te85LfhSlZme6QAHa2AABiyCWQQqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAExIJiEwyABKQAADKEmmmnZrRmIAu8CoVlbJVeK0UusevQyqYB2fDJlHGTTTWTWaejTPT7I25Gp91iF4pJwhUX4mrR315+8jly1vXmvMML1tXmrzdSi1qaz1dTZ3k18Uyn2lsqUE5xV4cfy/4L0z1tOl65IlWAA3aANlGg5PL1fA7I7Mb0kr9Vl/BWbRHlEzEK8ypxuyw/wBBxL9mk6nWFp/LM34Xs9jLv/bVv2SS+LyRWctPcf2juhzUoF5srY7naU/DT+Dl0X9lng9h0MNHfxlWn3lk40VOL3eTqSWX6V8Sv2p2khnuPeeiS9levI4b5rZZ7cUfllzaeFV2wxKni6u7lCG5QprgoUoKFl6pv1KUyqTcm5PNttvzebMTvpXtrFfTeI1AAC4AADFkEsghUAAAAAAAAAAAAAAAAAAAAAf/2Q==',
      key: '1',
    },
    {
      title: 'Comunication2',
      date: "Well-Being: Strategia d'impresa",
      body1: 'Il webinar è stato posticipato di 15 minuti. Ci scusiamo per il disagio! ',
      body2: '',
      image: 'https://pbs.twimg.com/media/EZug5BaXYAEsJ_v.jpg',
      key: '2',
    },
    {
      title: 'Comunication3',
      date: "International Women's Day 2019",                             
      body1: "Mancano solo 3 giorni all'evento più atteso dell'anno!",
      body2: "",
      image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAPEBASEBASFhUREBcQFxUXFRUXFRcYFRUXFxUVFRUYHSggGBolGxUVIjEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGhAQGy0lHR0tLS0tKy0tKy0tLS0rLjUrLS0tLS0rLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLTctN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcBBAUCAwj/xABJEAACAQICBAgICwUIAwAAAAAAAQIDEQQFBhIhMQciQVFhcYGREzJzkqGxwdEUFiM0NUJSU2JyshVUosLhFzNDgoOT0vAkRPH/xAAaAQEAAgMBAAAAAAAAAAAAAAAAAwQBAgUG/8QAKxEAAgIBAgQFBQADAAAAAAAAAAECAxEEMRIhMkEFE1FScRQVIjNhI0KR/9oADAMBAAIRAxEAPwCjQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbOBwFWvLVpU5zfNFN975CY5TwZYyttqyp0U+d68u5bPSAQUF0ZbwRYVWdWtVm+ZWjH1X9J36XBtlNJXeH1n+Kc36G7AH54Be+O0VyyO7CU13kXzLRvAbbUbdUpL2mcArAEmx2RUU/k5SXRsZoVMhrLxbS9DGGDkA+uIw86btOLi+lW7uc+RgAAAAAAAAAAAAAAAAAAGQdbIcgr46Uo0VHi21m2klcmuW8GcVZ4iu3+GmrfxMhs1FdfUzSU4x3K0Ojl2R4rEf3NCclz2tHznsLjy3RbBYe2pQi39qXGfpOylZWW5cn9ClZ4kl0IhlqPQq7LeDWvKzxFWFNc0bzl1X3J953VwaYO22rX86H/EmgKctbdJ74IndJkL/s0wX3lfzof8T1S4OsFCcG5VprW8WUlZ7HvtFPkJmc3P83hgaPh6kZSUJJasbXbldLfybTejU2ysSbNoTk5LmdXLcup0YqFKnGEVyRVkeswz/BYP5xiacHa+rdyn2Qjd+gpnP9P8ZirxhPwNN/VptqVvxT39xE5Svdve+XlO4XC7cZwwYGm2qNCvUtub1YRfSrtvvRG8x4XsRUfyeFpQX4pyn7itAATKvwh4uf8Ah0u6f/I0aullWfjU49ja9dzocFuGjUxVTXjGSjQbtJJrxorc+stH9nUPuKXmR9xTv1qqlwtEM7VF4KcoZ5Tb48Zrp3r3nfy7FUauyFSLfNufc9pYn7OofcUv9uHuMrL6C2qjS8yPuIfukfaaeevQiKwUKi1ZxUk+RpNHGzPQVTTlhpar36kneL6pb12ljVcBTe5WfRu7jzTwrRZp1ldv8ZJC2LKFx+Cq0JuFWEoSXI16U+VdJ8LH6LWAp1La9OErbtaKdu88Vcnox/wKVvJx9xtfc61nGUZnPh7H53sLH6D/AGdQ+4pf7cPcP2dQ+4pf7cPcU/uUfaR/UL0Pz5Ywy5tOsFSjgMQ40qaaS2qEU965UimWXKL1dHiSJYT4lkwACc3AAAAAAJfwZY108aoX2VoONudravaXAfn3KcV4GvRqJ21KkXfovt9Fz9AQldJ86T79px/EoYkpepV1C5pmQDJzSuYAABkifCd9Hz8pT9ZLCJ8J30fPykPWT6X9sfk3r60U4DJg9GXwAZQBPeCOHy+IlzUUu+afsLQK34IYcbFP8MF36z9hZJwde/8AMyld1mAZBTIjABkcwbOEqq6Uu87dOgmtqumiNnQwGPcVqSezkZ1NLq8/hMsV29pHxx+E8HLZ4r3dBqHUxVW6aZzGQ6zS+W+KOzNba8c0R7T36PxH5V+pFIl3ae/R+I/Kv1IpEueHfrfyS0dIAB0CcAAAAAAyWjlmn2Go4WjGevOpGkoySXKlbbJlXAitphakpGsoKW5YGP4TarfyNCMVzyes/cTjIqVeVGEsTK9Sa1mrWUb7VFLoRWHB5k/wrGRlJcSh8rLZsbvxI9+3/KXLiq8MPRqVqjtGnBzfK9m3tMR01UVyiY4IrYwlbYD44Gv4WlTqWtrwU7c11ex9jzs+plF7g081wFLEQVOtHWg53a3bk2t3SjcPFXfD838sjEW08owjhfErLv3ZedL3j4lZd+7Lzpe8kAJfPt9zNuOXqR/4lZd+7Lzpe8fErLv3dedL3kgA8+z3MccvU5+V5Jh8JreAp6mva9m3e17b+s39X/tzIIpScubNW2zGr/241TIMAgnCHpBicHVoxoVNVTpuTVr7dZrl6CJ/HvMPvl5qOxwuf3+G8k/1MgJ3tLTB1RbSLtcU4rkSX495h98vNRn495h98vNRGQWPIr9q/wCG/BH0Le0F0vljISpV5XrQ4yexa8fejp59Wr+Bn8Hnq1Ftjuadtrjt5ymMsxs8PVhVp+NCV+vnT6y2qWYRq04VIPZOKkujo7NxJwxaw1yMtFc5jpZjcRTlSq1bxlsa1UtzOCzuaW4FUsQ5RXFqcZdD+svb2nDZhQUeSWAklsYABkyAAAAAAAD64ei6k4QjvnJQXXJ2QBc/BZlHgcFGo1xsRLwr/Lugu7b2mjwx5r4KhRwsXtrPXn+WG5dstvYWLlWAjSp06cd1OEYLqSsUPwm5j8IzPEtO8aTVBf6eyX8WsAWvk3zah5KH6Ubhp5N82oeSh+lG4eYs6mc6W7B4q74fm/lkfQ4WmWazweFdamk5RnFWe7jXT9ZiEXOSiu5iKy8HcBU/9pWM+xS7n7x/aVjPsUu5+8ufb7SXyJFsAqf+0rGfYpdz94XCTjPu6Xc/ePt9pnyJFsmDSyWpWnQpzxCiqko6zUdyT2xXXaxulKUeF4IWAA3beYBXfCbllfEV6HgaUp2pO+qr/WZEfilmH7pV7i76S5Xve33I+hfr10q4KKWxNG5pYKIq6L46KvLC1Uuo51bC1IePTnHri0fog+VahCatOEZfmSZLHxJ94m31H8PzuiXaHZg/BzpN+I9ePVLxl37e0m2caC4Oum4R8FJ8sd3aiDYnR7EZbiISmtanJuHhFus9nG5uQvU6quzbcmjbGR9tKKfhKLfLB6y9pDWTWqnUvHnViGVoOMnF74trudiyzdHgAGDIAAAAAAJBoHhFWzLBwf3yn5ic/wCUj5MeCdL9q4dv6sakv4JL2gH6EnONOLb5FfuPyjjK7qVKlR76k5TfXJtn6G0vzJxwuKae1Yeo116krH5zAP0Dk3zah5KH6Ubhp5N82oeSh+lG4eYs6n8nOluzJE+E76Pn5Sn6yWET4Tvo+flKfrJNL+2PybV9aKcMowZR6MvglfB7kXwrEqc18nQtN7Njl9WPt7CL0qblJRiruTskuVvci9NFsmWCw0KX1vGm+eT39iKmsv8ALhy3ZFbPhidcwDJwCkYPE9r1e1+xHqTsrmKcbK75dv8AQA9AAzwv0GGAZsYMAyfOrSjOLjOKcWrNM9gJtbAh2PyFUJ3h4kns6OgrTSOj4PFVo80r96T9pfNekpxafKU1whUdTGy6acX617DvaPUOyOHui5TPKwRkGTBcJgAAAAAASfg6r6mYUnzxmv4WRg6ejtfweKoy5p285Ne0AtLSnF6+HxEV9ajNLtiymyxcxxt7rnK7nGza5m13GWD9AZN82oeSh+lG4aeTfNqHkofpRuHl7Op/JzpbsyRPhO+j5+Up+slhE+E76Pn5Sn6yTS/tj8m1fWinDKBs5fg51qsKcFeU5KK7eU9G3gvkx4Mcj8LVeJmuLS2Qvyz5+xestM0smy2OFoU6UN0I7el8rZunndVc7bG+xQsnxPIANfMcZChSnVm7RhFv3IgSbeEaIi2nWlssG4UqKi6jWs3K7UVybE95XeY6T43EP5TEVLc0XqR82NkzTzfHyxNapVnvnK/UuRGmeho00K4rlzL0IKKPU5uW9t9e31mYVHHbFtdWz1HgXLGESYJBk2l+MwzVqspwvthUesuxvbHsLV0a0ipY+nrQ4s4+PB710rnRRVzvaFZlLD42i09k5qnJc6ls9bKep0sJxbS5kVlaaLwMAHBKQKk4UsNq41T5KlJehu/rRbZXHC7T24aXRKPfZ+wu+Hyxdj1JaH+RXJgyYO6XQAAAAAAeqc9VprkafceQATjDLXSlvuk++xF88w3g681yPjLqZJtFanhKK56b1PavQz5aaZf8nCql4r1H1PxfSbPYwWjk3zah5KH6Ubhp5N82oeSj+lG4eWs638nPluzJE+E76Pn5Sn6yWET4Tfo+flKfrJNL+2PybV9aKeRZHBfkdtbFTX4aftkQbI8rnisRTpQ+tLa+ZLxmy98FhY0acKcFaMIqKOpr7uCHCt2WLp4WD7AA4hUBXfCpmk7U8PFNRfHk7bHbdFPl5ywqkrL0LrNTMcpo4mk6VaKkny8qfOnyE+nsjXNSkjeuSUssoBmGSvSbQmvhW5006lLfdK8o9El7URVo9DCyM1mLLyknseQZsLGxkwdDIablisOlv8PD0STfqNFRJ7wa6OTlUWKqxahDxE/rP7S6CK6xQg2zWclFFngA813OeCuuF2fzWP5pd1l7SxipuFTFqeLhBP8Au6Xpk9q9CLugjm34JaF+RCjBkwd0ugAAAAAAAAHe0PxypYiMZPi1eJ1P6r79naWRXy6NalOnJbJxcSmk9pcmhmZrGYeMr8eFoTXSt0u0znkCQ5bRcKNKD3xpxi+xWNgxHcjJ5ezrZzpbsyRPhN+j5eUp+slZydJss+F0oUXulWg5fli25eq3abUSUbIt9jMHiWSPcGWR+CovETXGrbIpraoLl7WTg804KKUYqyilFLmS2JGRfa7JuTE5cTyDIPlXqqNldKUnqx6Xbk9PcRLmaiO135I7F18rPoYjGySRkAP+hH840NwWKblKm4Sf1qb1X2rc+4kBk3hZKHOLMqTWxW+J4Lnd+DxStyKVP1yUvYfKnwX1b8bFU0uiEm+5tFmgs/X3epJ50yIZRwe4Sg1Ko5VZLbxrKF/yr2slsIqKSSSS2JLYl1IyCvZdOx/kyOUnLcyYAIzB88VXjShOcnaMIuT6kigs5xzxGIq1X9eba6t0V3JE54S9JFL/AMSjK+29Vro3Q95XTO3oKHCPE92W6YYWTAAL5OAAAAAAAAADt6JZ7LA4iNTfB8WceePP1o4hlAH6Iw2IjVhGcHeMlrJ9Z9W7K7Kq4OdNPgFRUq/GoTe3lcH9pdBevhqNWmpQ1JQnHY1ZppnMl4cm85K70/8ASM/C6X3kPOR6p8ZuXYurlfayM5lweUfhkK1KVqOtrzpdK2pRfM3bYSpIp6nTqlpJ5yQ2Q4AACmRgqXhC0glWxShSm1HDtpNO3H+tJNdxPdMs5WDws5J8ed4Q63vfYUjObbbe1t3Ot4fRvNliiH+zLF0Y4Qt1PGdCVVfzL2lg4bEwqxUqc4yi+VO5+d9Y38szjEYZ3o1JR6OR9aJr9BGfOPJm86U+aL+BWmV8Jk1ZYiipbPGhsfc9hIMJwgYGdtaU4P8AFHZ3nOno7o9iB1SRKwcaGleAf/tU+9+49rSfA/vNO72Wu79W7eReRZ7Wa8EvQ6wPKqXSaUtqvut6zXxWL1IuTaikrtt7jeGltl2MquT7GzOaim5NJLa23ZLrZX+mGniSlRwbu3eMqvNyNQ5+sjmmWlU8XNwpyfgovq1nz7OQi1zo6fQKH5T5ssQpS5s9Sk223tb3v+p5YuYOiTgAAAAAAAAAAAAAAGUyX6F6aV8DJUmnUozlbU+sm9nE9xEETTg1yLw9d15riUXdX5Z8ncaWTVcXJ9jWTwsltVcTdJ7VdXs966+k0Z4vVZuTimrM5mNwU98NvRy/1KVeqquXBYiFWRnyZuUcbTly2fMzYv8A/SF4uq02nsfpNOrmlWMJQVSWrJOLV+R9e40n4cnzg+Rh0p7EY09zv4XiWoviUuJHmdt7IyzvzyFSfEm11q/qsZWiNeXiSpvrbj7DowrUIqK7FiKwsEeMolFLQPGy3eC89+46mB4LMZUfHrUIroc5Pu1V6zYyQNGS4sq4GqKd8TiqklzU4qn3uWsS7LND8uwW2jhoa325/KS7HPYuywBSmQaEY7GWapOnTe3wlROMbfhT2yLN0d0MwuXpSt4SrbbUktz/AAR+qiT47GxpxcpSjGK5W0ku0rzSPhFo07xwq8LLatZ7Ka6eeQBKM5zelh4OdWaivS3zLnZUmlOldTGNxjeNJPZHlfTI4+aZnWxM9etUcn07l0JbkabZkBswAYAAAAAAAAAAAAAAAAAAB6itpamj+lOWYTD06UasuKryfg57ZPe9xVVxciupjasSNJwUlhlzfH7LvvZeZP3D4/Zd97LzJ+4pgFb7fV/TTyIlw4nTPK6qtObf+nO/fYgOZZ/F1p+Ch8lrcW7etbtI7cXLFVMaunJvGtRJbgM9wy8fWj/lv6iS5dnmCf8AjwX5uL6yrQT5NsF7YPPMArXxmH7akfedGOmmVUVxsXTf5eN+k/O4MGS+My4WsuhH5JVaj3JKGqutudvQQ3N+FfE1LqhRhT/FJucrdC2JPvK6uLgG9mecYnFPWr1pz6G+L2RWxGjcwADJgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//Z',
      key: '3',
    },
  ]);
  return (
    <View style={styles.container}>
      <View style={styles.searchBarView}>
      <Text style={styles.searchText}>Notifiche</Text>
        <TextInput style={styles.textInput} 
          inlineImagePadding={10}
          inlineImageLeft='search_icon'
          placeholder = {''}
          placeholderTextColor={'grey'}
        />
        <Icon1 style={styles.icon} name="search" color="grey" size={25} />
      </View>
      <View>
      <FlatList
        data={review}
        contentContainerStyle={{paddingBottom: 40}}
        renderItem={({item}) => (
          <TouchableOpacity
            transparent
            onPress={() => alert('Button Clicked!', item)}>
            <Card1>
              <View style={styles.card}>
                <Image style={styles.image} source={{uri: item.image}}/>
                <View style={styles.textView}>
                  <Text style={styles.dateText}>{item.date}</Text>
                  <Text style={styles.text}>{item.body1}</Text>
                  <Text style={styles.text}>{item.body2}</Text>
                </View>
              </View>
            </Card1>
          </TouchableOpacity>
        )}
      />
        <Button title="Click Here" onPress={() => alert('Button Clicked!')} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'black',
    height: windowHeight,
    width: windowWidth,
  },
  searchBarView:{
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'space-between',
    width:windowWidth/1.05,
    
  },
  searchText: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'verdana',
    fontWeight: 'bold',
    alignItems: 'center',
   
  },
  textInput: {
    
  },
  card: { 
    height: windowHeight/12,
    width: windowWidth,
    flexDirection: 'row',
    flex: 1,
   
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 2,
    marginTop: 8,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.8,
    
  },
  iconsView: {
    flexDirection: 'row',
  },
  icons: {
   
  },
  textView: {
    flexDirection: 'column',
    justifyContent: "center",
    height: windowHeight/4,
    width: windowWidth/1.3,
    marginLeft: 10, 
    marginTop: 15  
  },
  dateText: {
    fontSize: 12,
    color: 'green',
    fontFamily: 'verdana',
    fontWeight: 'bold'
  },
  text: {
    fontSize: 11,
    color: 'white',
    marginTop: 2,
    
  },
  image: {
    backgroundColor: 'black',
    height: windowHeight/15,
    width: windowWidth/8,
    borderRadius: 150,
    overflow: 'hidden',
    marginLeft: 10,
    position: 'relative',
    
  },
});

