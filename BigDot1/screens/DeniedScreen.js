/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {Button, View, Text, StyleSheet, TextInput, Image,TouchableOpacity, FlatList} from 'react-native';

import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
Icon.loadFont();
Icon1.loadFont();
export default function ApprovedScreen ({navigation})  {
  const [review, setReview] = useState([
    {
      title: '#GreeningTheDot',
      date: '03 Mar 2020',
      body: '',
      key: '1',
      alert: 'Communication canceled due to drafting text inconsistent with the template',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTP5X4yR5CEWvDPVgn_IhkA4Y2c7LY-j9y0CCRRYBa-_DTCrIKb&usqp=CAU',
    },
    {
      title: '#BrightStart         ',
      date: '24 Apr 2020',
      body: '',
      key: '2',
      image: 'https://www2.deloitte.com/content/dam/Deloitte/xe/Images/promo_images/Careers/me_brightstart-promo.jpg',
    },
    {
      title: 'Design Thinking   ',
      date: '12 May 2020',
      body: '',
      key: '3',
      image: 'https://image.isu.pub/171106173230-c2cdf288f554dba401b200d464689e12/jpg/page_1.jpg',
    },
  ]);
  return (
    <View style={styles.container}>
      <View style={styles.searchBarView}>
        <Text style={styles.searchText}>     Denied     </Text>
        <TextInput style={styles.textInput} 
          inlineImagePadding={10}
          inlineImageLeft='search_icon'
          placeholder = {'             '}
          placeholderTextColor={'grey'}
        />
        <Icon1  style={styles.icon} name="search" color="grey" size={25} />
      </View>
      <View>
      <FlatList
        data={review}
        renderItem={({item}) => (
          <TouchableOpacity
            transparent
            onPress={() => alert('Communication deleted to drafting text inconsistent with the template', item)}>
            <Card1>
              <View style={styles.card}>
                <Image style={styles.image}  source={{uri: item.image}}/>
                <View style={styles.textView}>
                  <Text style={styles.dateText}>{item.date}</Text>
                  <Text style={styles.text}>{item.title}</Text>
                  <Text style={styles.text}>{item.body}</Text>
                </View>
                <View style={styles.iconsView}>
                  <Text style={styles.icons}>Denied</Text>
                </View>
              </View>
            </Card1>
          </TouchableOpacity>
        )}
      />
        <Button title="Click Here" onPress={() => alert('Communication canceled due to drafting text inconsistent with the template')} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'black',
  },
  searchBarView:{
    backgroundColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft:160,
    marginTop:10,
    width: 413,
  },
  searchText: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'verdana',
    fontWeight: 'bold',
    alignItems:'center',
    marginLeft: -65,
    marginRight: 255,
    marginTop: -5,
  },
  textInput: {
    fontFamily: 'verdana',
    marginLeft: -110,
    marginRight: 10,
    borderRadius: 30,
  },
  card: {
    flexDirection: 'row',
    flex: 0,
    width: 400,
    height: 90,
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 40,
    borderRadius: 0,
    marginLeft: 40,
  },
  iconsView: {
    flexDirection: 'row',
  
    marginTop: -5,
    position: 'relative',
    justifyContent: 'flex-start',
    fontFamily: 'verdana',
    fontWeight: 'bold',
  },
  icons: {
    marginTop: 70,
    marginRight: 190,
    marginLeft:20,
    position: "relative",
    color: 'red',
  },
  textView: {
    flexDirection: 'column',
    alignItems: "stretch",
    justifyContent: "space-between",
    marginLeft: 15,
    marginTop: 2
     
  },
  dateText: {
    fontSize: 11,
    color: 'grey',
    fontFamily:'verdana', 
    
  },
  text: {
    fontSize: 15,
    color: 'white',
    fontFamily:'verdana',   
    fontWeight:'bold',
    

  },
  image: {
    width: 130,
    height: 90,
    borderRadius: 15,
    marginLeft: 80,
    position:'relative',
    opacity:0.7,
  },
});
