/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
} from 'react-native';
import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class TagScreen extends Component {
  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('PurposeScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                    'https://images.pexels.com/photos/3787903/pexels-photo-3787903.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Deloitte Purpose</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('DiversityScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                  'https://images.pexels.com/photos/3183197/pexels-photo-3183197.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Diversity & Inclusion</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('WBScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                    'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUTEhMSFRUQEhUQFRUVEg8QDw8PFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rNSstLS0tLf/AABEIAHABwgMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBAIFBgEHAP/EAEAQAAEDAwMBBQUDCQcFAAAAAAEAAgMEESEFEjFBE1FhcYEGIpGhsQcUQiMyUmJyssHR8BUzU4LC4fEkY5Ki0v/EABoBAAMBAQEBAAAAAAAAAAAAAAIDBAEABQb/xAAlEQACAgICAgMAAwEBAAAAAAAAAQIRAyESMQQTIkFRMmFxgSP/2gAMAwEAAhEDEQA/AMVPNtOAUF8rnDAK6+QE5TEczWhRtV9bPEbKh9O7ql5IlezVbXCwAVTUO8FVhySfaoOMndCRan6WI2S8Yucp0ThoTcjfSDnJ9IfpIrcpt0tuCqVlQ53CdbE9SZIK7kzk6Rb0tVfCsoSVnaW7TlW7KrGFLkjvQxSRbRSBSqW34VE2rIdlWsE5IXLGlsdBplXqMe19/wBIX9Rhb/7Pvai9oZDkYBP4gsTqcRI8sqso6osc1zTYtIIQJvHO0enhkpQpnp/tzo7RKJQPdm57hIOfiM+hWMrtPBGF6K6cVmnlzcuDO0HfvZkj6j1WA7coskXy5RZD5GPizH1kW02XGHCu9Wod2Qs+5pabL0MU1OJHRIosIKCwq70+na4ZWZGooW1sr5qmyAycnvVjV0I3YT1Bp7Tyg5QUbNKmnaHFaDTmhqVqtN2G4XQfNTZFz66CRZPkBQnxkhLRSJxjzZBwcTlsoayjIN1oPZXTQ6xI/WPn0+X1QpGXBvwrz2QHulx/ESfTom+2TjTPR8LFcuTNXA/a23cvOdare1qHuvcA7Gnwbj63Wt9odUEUDnD84+639s4HwyfReXtkI4WwjyQXnZKqH/S9DSQkqulKjFWnhOxS7ggalB2ea2mUHZZyiRNAKPWDa5DaQcprm2hD7JTwghUdXBYrQMIsq2tIuiwTalQMtbKUtK+aDdOloUZGK7mcshwMuh9gbqbHJhjwhbaOti3YFNU0K6TdMRvACVkyOqAcrYyDYJB7Moj6juQXSXSYxYMnZPtOijgIL3WUO1umqB1NhnPugTlcDTdELLo1FI1JJkIpUZxUGxhELcLm0c2r0QaplyiAouesOJ7lHehlfRlEloOOkNNkwuwvF8qMbQvpGAZS3TCUuXY+JGr5Vv3jxXUPoGUQc3K+eb4uoVBdfCLSUxOStqlbFNV2Tp6Ak3To00FOU0Ytym4YWg5Uss7sJKygn0U8hJVVAQMhbGSZowq3UXsOEePPNvYxIotPkDTlO1Gojhqg/TiRgKVLppv7yLJ675Mxp0HomGTpZPx0ZaUekjDUZ8wUM8km/j0HFaFXQG6Nuc0KfbhfduDhdHJL8DWjsdVcZVDWnZIQOCbjyOVetjCrNapPxjy/kU2TjNaLfGyfKjffZnqnuuiPQ39CqLUIDHNIy1uze5o/Zv7vysvvs2faf/KFsftB0oWZUNHNo3+P6Dj48j4LYJuL/op8uNxTMexzSLFKVOmNdeyk9hCPC5b/AB2jzJIytbpzmHHCZ0/d4q+ng3Ko1KcQ4H57sD9TuPmqMeT2fFi44nJ0HqHbDZ5DTjBPv2P6oypU9U0cOHNrZBv5FI6fpD5XRkAndY95N3G30+S0dX7JSOB2i5BJFhnobrXw6o9BeCq7IzS7m8dFQS1WbK7dRuZ7rvQ5yFR6jprgdwS4cU6Ic2J45UzrSeQrKkmKoGVZBscKzpakIsqaQtIsNQls0gdR/X8Ve+x1T+RItchrreJHCp6fTnTnua3Hi49VptP0sQgchtiTYc4OFNk1SPf8XHxhsyntJVF5ZGDfZ77j+u7geg+qrqalzlfTxEPLj+Ik+Av08kxFUAJsnxikjxc+T2ZHISrhtOAuQ1ZHqnpGByE6jCH2JqmTtfgrVsL8qDbAK1jiFrJSrob8LI5F0DJfYpusq6fJT0lK/ohfdXDJTsbSfYmVv6ES1THC+ecqAVPYKv7PixMwUd1OniHKZfKAk5MjekEq+wEkIAScjkWoqbpYlbji+2C1sGSVxriplQDgqA0ELboPZkFHa5SD1ibRik0RaVCVxRJAhXXR/TY/p2Mko11CNDqJF1W6Nq3RJ8wQw8JZxRqdiPiooLgoodY24UHwdV2MqwiiuFNJuIpXZUEuC4JCQtCzSgQkZdP2nhFHPFj9pbRS9mV1XH3ML5M9yO9j/DlaQ02skH1R6J6peHOQ56dozhJi0qsFtdnKbtCOqO50o70Gnr9psrmmrI3DKTkcou+JiRVNnd1RKe5crKsYy2LKrjlsStjPlF0gk2W76gNCG2oDuFXzz3Q4p9qT6Qnk+i5dIAhOekPvO4orjhcoU9mqVjIymIoOqSo3d6e7RZl1pBoLGbFdrmbo3eAv8P6KWEuU0x+4W78JDg1sbjdSTGvYF9piPAL1zUqXt6V8fVzDt/bGW/MBeJaBP2dQw9ztp8jhe4aRPdoVWB9o9nIrgeRRyDr80wGhM+02jdnVSgcFxkb+y/3vqSPRUI3tdYqWat1Z5f8AG1IarJwwFxvYW4G45IHHVYHUqxznlx5LifLOFrNbqbROubZb6ncDb5LDVvff/lW+PqA/CtXRsdP1gsgvG78LGmxO5hBJZ5fmkfFN0PtPKJGkONtxPJzza/qT8FkdAqmAvZIdokYW7rE+8MtGPFXFBTF5u2xBJBIPBuQRlHK0tFcHfZ6lIxlRTiYY2nNrZt/M4WN1Cu2uLHdMFXMVf2FMIyM3vtFsm2Lk8/RZevpX3ErhiW5DgbhxH51j3i4+KWoqT2I86N40xaopQ43CYgprC56Z+C7DMExK67cdTZMknR5/jw5TSNn7H035IX65V5rQDIHn9Ww8zj+KT9nWbWNHgAgfaBW9nTtt+OVrfQBzj+6EjjyPcyy4wf8Ahka6kJF1mXvLXWWuo64PZ42WQ1bEhRYbtxZ89NBWzlFNW4IVO0WXKlqF8eVCXZJuom6sqap3BZvblWNJcIsmJNWgYyLZySr6izThM09yuVdJcJWNJS2N7RmN+4ohFk4dNIyFX1RINir1OM3URM7kyTZl2SRJdooOmKb6zvVYaV6GJkIhDITFFDYwQ32l0MA3Q43WRBLldVHNV0GCM2yA6YKLHk8IHFsW4NjgUC1DBK6HoOLQHFolsQ5o00w3XJCAsTdmKTsQbEUWOMorG34RQLIpzDlNkWwHxTtGx1xyu0so6pxsg6KOeWS0zYxvY7E87UvUsLguNnKn2+OEpS2OvRXCJ66nO1C+TfYxXFGYmqveUX1d0WspwFXPwroxixsYxZMvKPFIR1SwKNA0uNgiklQTQ2ypPUlHaeqGKbbyuySiyTSf8QGtaISyIdyUN0t01TkLpfFC2q7D08DuU2HEDK7DWNQKmqvwo+UpS2jU6CbzyiwznqkxMvmS5TFDl2goO2WRKlFObpRkl01GEuceOmOOTna+/k71XsHsbqAfG035AXkFYMA92Fd+yGtuieI74cceBSYS4uz2/HlzxpM9G9vaK7GztF+z919v8M8H0P7y82rZbnC9l0+pEjLHIcLZyMheFVdTYkcFpLXN6gg2+GE6eJSfJEXkY3HYnrrSYvDeM92CslXH8J6fPuW9oq0cLN+2NJ+U3D8YB9V2PUqYWHLcOP4A9kYm73Pfa4G1l+jiQC7+u4q8qKOUQySRgfkrGVpAddh/HbvHf3eSykE2wjuaWn/wyfrdaXR9YLZLON2ytMTx3gEj/Sfimy/SmHVFRFWSOc0FzyHENAD3EejQbeivpqssg7N34pN4uSQLNLXW8/dHovtG0YNkkec2LmN7snJHpfHiUzqum7hj/hCq5r8FZs6hjlBrbKuhfda3QNNMsjbD3WZPdc8LL6dp0he2NouXG3gB1J7gF7V7O6a2KNrB0AuepKbm29CvCjtz/CUFLtAWO+0mTcYo/wBEOf8AGwH0K9HrIwAvMPbB26qcP0GNb8t3+pTyfCRR5M//ABf9mVpZywlJV0oc66ZrQASqSSSzlVCCl8keLK30OhxHCZZMXBVjKpWFJVgYIGUvLjf4JcZJkYxk3snqQ3NsJarb+JoQIKkhwutiuUTkjSsYG5XZJwQhQHe1KzsLfJS5I7DUqR8Z8qs1Sl3ZC+qKmxTNNPdalKHyQvlbM3JEWnK4+PqFotRoQ8XCqTSkcq/FnU1/YalXZXsuVJ8JTBaAUyGghMc6Oc6eiqMZUSrJrBdckpAt9n6d7V9lamaYFSEOVYU8QshyZEkdPIqE5XHuXGhM1DQFCAXKFStC+Vo+juiiI9UcABRdMFlt9A9gmPAK7JID3IMpvwuwQk8rJRXZrhq2yO/KbjnspfcR3oEsVsEoG4y0FaSGm1iMKoFVgYeiZghcUuWKK6CbpaGO1Xyl93XyDiZyF46UOVfqNDtVq2MjIUKyMkcLYZWpdgKTjszIYU9RnYUWOEXXZwFZKalofLImHqarcFWPf0RS5BcF2OKidA4wJmN6CwKRZZbKmdKnoaauPBQ4p7Jp0gISWmmAk0RhF0UtsgsfZGbJcodp2FGLuxiI2TLJLLkTQbcIkkVxhTympPYxHz5ARZDpX7XA9xuvmU6KaY8rPUm9MrxeV6o7VnpGm+1UUMIkkdgDDRl7ndwC8s1arMkz5QNvaPc8DnaHG9kZ0feUvVMuqcePiibyPNlmfVIf05wcLgZBsQoe0EW9rfC6RoJ+zeCeDg+Xen6iqY8FoNz0AucpU4NS/ob4s/kjFyA2efP4m4x8FYae8lwHW9/I3H/0UEw3cW8flLHyG66b0yAtfwf7xvnZzmC3yRPo9OPZvqEf9NERa7t5f4uDiwZ/ylBBde1r3PHeUzplORDex2se9lw0uAO4kXPS97p2igIO5zSOjSQQLnqe7z8UiEZTnSWiTyovlbLX2c06x3WG485yPAGy2VJHtz/x/sqvRKY2yLfD6haCNi9P1xSoTik47QrVuNjcH0Bd9F43VVbpqiV1sGR1v2Qdo+QC9n1Wo7KCWT/Cje4eYB2/Oy8YopGs5+PevP8AJio9B58zlFRYpqmmucLhULaB273gt82oa4JKspmngZS8PlOHxZHLXRmWaeEydNHIRZ6J4S0dSRg9FXz5q0xUrGBIBgrstIwi4S0x3cKVITwSg41sGgzKos4vhTdqAcMoc7AlnRCyCTTQLbQjUm7lYQOFkpHECVb0ULe9FkaUaNidZIRa4wmZqUSMumZIWlmEjR1RYS0qByfcRtL7KiXTSDlKvhLVqahu5VGpU5AvZW4fI5UmLcKKSS4KPE7CE5t1wNIVr2jeKaJvUGyopAKGYwhS/RaX6SJuob9qX7QhFjiLkajQxRoL96Q7klTkoiMotOy65yilaMckuhiBrQMohF/zUOVllyKN5yFPd7FrZEvIPKLK24uhshcTYhWA0xxC6UoxqxtaFKJ7Qcq6ow0qmGlPaeVZ6dAb2S8ko1aYSGTSjuXU1908Svkj2IPiK01IBgqdRpzSEqyd3cVyprXM5U7Ur0c+NFRXU4juqeaRXNW4yKEGlZuVfjyqEfl2KVXpFZTUrndE3NprgL2WipKdreibLWEdFNPzp8tLQzhZgyCDwplt+VsP7NjJ6IVXpDbYTF58bpo5wZkHQpmNtwjz6c8GwBRo9PeBwq/fBq7MTYFsKiYrcKM8cgPCgZXDkFat9M7i/plhTTBMmcKnYb96ZjS3hV2GlQ+Jf6umqeYE7XdRjzVS15CIJCVihTNatFhIyx6fH/ZLSNPgiyi1x0Of5IDz6/JNjIhfYGSO/wDQQXxWyOnoUdwXKgEAk46+CNOw4tpopWTWeXc3lvbk+6R/P5K+iA2iUW/vWsx/2xdp9QPqshG8j3vHd6jP8FeQVFi5p4N7fq5u0+BFyPUqeR9Hj0ket/ZnViV08R5LY5QehFtrh8dvxWubpBaTaxaeWP4/yu6eRXn32NB3aPcRhsO2/wCs5zSB/wCpXrZkxiw+YKowWoEvkxUpidLS7MC4H6Jzb9lw5Hgm1MuuohMFpUZT7R6/ZS9mOZnAeUbSHOPxAC8nknW19qq0Tzv/AEWfk2+TSbn1Nys3/Z4uvNy5YuTbJ8ibZ3T58J3eEONjWhd7MFRzkmzqJyMDgs7qdKQSQFpWx4SVa1oXYcvCWgZ9WZtjiBnogPqCrp8QdhVdVQuB4K9THljLsQpK9hKb3uUwy17JajpXJ0URBugnKN0BJ29Cz4CDeyGJXA9Qr6ljaeVOsom2wkvJ9M3iIUVYRyjyAE3tyu0lMCm+zDR0Us6vQxLQOhb7wuFaatRtMfHRJU0rQVZGYOFktxaYcWqaMBPT7ScJeUXWt1ikBHurLyRkFepgzKa32KVoTcCEASElX0dKHBT/ALDvxZOeeK7NT/op4ILlW0NJt6Lg09zDwVc0sO4ZScmf8Bq2VUzgRYhBpWtB6K4rdKuMLPuontOeiXFqV7BcKNHDRscEx90a3uVDDOW9U5HVl2ElqUfvQxUOzQstccoVPUk47kpPIWjkpSCqPRGoclZnKi5Jv0S8U212Eu2vt+cgtrASthie7CsvPvRXyq+28F8t9SC5nIq9ruEjqhc7hJUNO4ZVuxt+iW1HHK0ZJ2I0cwbynPvNzhSOmg5spNpg1ZKcHs5Jk2vxyixjd1S5lbwSExSkDqly66DTJbXDqmI6kdSgTzJB9ycFLUOXZrLWSaNSZVM8FSyROskzA++CmxwQfbMUjQTPjd0SFVSxlLQRkcpiWG45TIpQemddlZPAG8LkDkvUtLTldp5FfXxNqx/siVwgjomqSqHVMSStPQKdykntGqJy92NPhb1GErI1PwWIIHQ3t0ylJ2W7/qnYtkuVUxV480pqclo3c5FunJwnCh1LiGtIsbSNNum7O2/husnNUrCwpPJFf2Zd0RAIPO2/rex+qtKalc5hcOt2DvuC2+PX6pnUKMD328F/ZZyRZzGZ8cOKPSv/ACUjeNs2bHJByP4hTSej6CCPU/s7oWijuZpGOkkLjsDCdrfdzcHFw4+q1sLIxjtKqQ+AeP3bBVPslJIymgDY2W7Fly5xLnO2gk2AxknnvWrpqknlrR5H/ZWxVRR5kvnNsBBFm4Y5o75JHPefJocR8T6I1ZNsje/9BjnfAE/wRSq32tqBHRTu74zGP2n+4P3kMnoNKkeR9ubeKC6oKF2iGZQvH4CGybpzdMRz+KWwVFrM8rqjQNlpHIVIwb+UKBwtlFbJYqWS3o3/AE+j0+xUp6fCL97UTPdcnNHOEWVgjcD4JiSTHojyShLStvlaszvYmWKuhBkzg6wGE1VudtXY25TL3XCoeZWhaTRnoq8sOVZRVIkGFRaxEQ42XNOkI6qvJiUoc49hVqzR08BBVg2E83VTFVXCsIK3FlBJyfZsWhgeNktVaa14uBlBdOb5TjamwQW4u0HaKmTSyOFHc6PvT0ld0QZpg4ZVinJ/yQv/AALSVG4ZCZiOUjRygJwzBKlHfQxE56kAKirZ3E4CfnkQgLo4xrdHSRSPxyj08wQNTweCk46kKtY+cbFuLq0aOGQOwbJetswYskqOQ8purG5iVx4SM77KOeruUWkdcoJprFW2mUIwVZOcYxG6rQXafFcVt2LV1Re4Gj//2Q==',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Well-being</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('EventiScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ6r8z2s7TcDQuQIVsFS_3UFM1FjpKvuqv5jg48W1ArK2vEv9tu&usqp=CAU',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Events</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('TalentScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                  'https://images.pexels.com/photos/4078457/pexels-photo-4078457.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Talent</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('POScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                  'https://images.pexels.com/photos/3410513/pexels-photo-3410513.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>PO’s corner</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            transparent
            onPress={() => this.props.navigation.navigate('ConsultingLeaderScreen')}>
            <Card1>
              <ImageBackground
                source={{
                  uri:
                  'https://images.pexels.com/photos/2096578/pexels-photo-2096578.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
                }}
                imageStyle={styles.image}
                style={styles.textView}>
                <Text style={styles.text}>Consulting Leader’s corner</Text>
              </ImageBackground>
            </Card1>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    fontFamily: 'verdana',
    height: windowHeight,
    width: windowWidth,
  },
  searchBarView: {
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'space-between',
    width: windowWidth / 1.05,
  },
  searchText: {
    color: 'white',
    fontSize: 17,
    fontFamily: 'verdana',
    fontWeight: 'bold',
    alignItems: 'center',
  },
  textInput: {},
  card: {
    flexDirection: 'row',
    flex: 1,
    height: windowHeight / 7,
    width: windowWidth,
    backgroundColor: 'black',
    shadowColor: '#696969',
    shadowRadius: 4,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 2,
    marginBottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 40,
    borderRadius: 50,
    fontFamily: 'verdana',
  },
  iconsView: {
    flexDirection: 'row',
  },
  icons: {},
  textView: {
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 12,
    shadowColor: 'black',
    shadowRadius: 0,
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 2,
    marginTop: 6,
    fontFamily: 'verdana',
  },
  dateText: {},
  text: {
    fontSize: 26,
    color: 'white',
    fontFamily: 'verdana',
    opacity: 1,
    position: 'absolute',
  },
  image: {
    height: windowHeight / 4.5,
    width: windowWidth,
    borderRadius: 25,

    position: 'relative',
    opacity: 0.4,
  },
});
