/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  RefreshControl,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import AsyncStorage from '@react-native-community/async-storage';
import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
import {ScrollView} from 'react-native-gesture-handler';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');

export default class SaveScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataCard: [],
      color1: 'grey',
      loading: false,
    };
  }

  selectItem1 = item => {
    item.item.isSelect = !item.item.isSelect;
    item.item.selectedClass1 = item.item.isSelect
      ? styles.selected
      : styles.list;

    const index = this.state.dataCard.findIndex(
      item => item.item._id === item._id,
    );

    this.state.dataCard[index] = item.item;

    this.setState({
      dataCard: this.state.dataCard,
    });

    AsyncStorage.getItem('like')
      .then(array1 => {
        if (array1 !== null) {
          const card1 = JSON.parse(array1);

          if (array1.includes(item.item._id) !== true) {
            card1.push(item);
            AsyncStorage.setItem('like', JSON.stringify(card1));

            alert('I like it!');
          } else {
            const postsItems1 = card1.filter(function(e) {
              return e.item._id !== item.item._id;
            });

            AsyncStorage.setItem('like', JSON.stringify(postsItems1));
            alert('Removed like :(');
          }
        } else {
          const card1 = [];
          card1.push(item);
          AsyncStorage.setItem('like', JSON.stringify(card1));

          alert('I like it! ');
          {
            [styles.selected, item.item.selectedClass1];
          }
        }
      })
      .catch(err => {
        alert(err);
      });
  };

  onChangeQual(i, type) {
    const dataCar = this.state.dataCard;
    let cantd = dataCar[i].quantity;

    if (type) {
      cantd = cantd + 1;
      dataCar[i].quantity = cantd;
      this.setState({dataCart: dataCar});
    } else if (type === false && cantd >= 2) {
      cantd = cantd - 1;
      dataCar[i].quantity = cantd;
      this.setState({dataCart: dataCar});
    } else if (type === false && cantd === 1) {
      dataCar.splice(i, 1);
      this.setState({dataCart: dataCar});
    }
  }
  removePost = async _id => {
    try {
      const card = await AsyncStorage.getItem('card');
      let postsFav = JSON.parse(card);

      const postsItems = postsFav.filter(function(e) {
        return e.item._id !== _id;
      });
      alert('Removed from Favorites');
      // updating 'posts' with the updated 'postsItems'
      await AsyncStorage.setItem('card', JSON.stringify(postsItems));
    } catch (error) {
      console.log('error: ', error);
    }
  };

  _onRefresh() {
    this.setState({loading: true});
    this.componentDidMount();

    this.setState({loading: false});
  }
  componentDidMount() {
    AsyncStorage.getItem('card')
      .then(card => {
        if (card !== null) {
          // We have data!!
          const cardposts = JSON.parse(card);
          this.setState({dataCard: cardposts});
        }
      })
      .catch(err => {
        alert(err);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewSearch}>
          <Icon2
            style={styles.icons1}
            name="ios-search"
            color="grey"
            size={23}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Search"
            onChangeText={this.handleSearch}
          />
        </View>
        <View>
          <ScrollView
            style={styles.vieww}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh.bind(this)}
                tintColor="grey"
                title="loading..."
              />
            }>
            {this.state.dataCard.map(item => {
              return (
                <View>
                  <TouchableOpacity
                    transparent
                    onPress={() =>
                      this.props.navigation.navigate('ComunicationScreen2', {
                        id: item.item._id,
                        title: item.item.title,
                        data: item.item.date,
                        img: item.item.imgUrl,
                        pdf: item.item.pdfUrl,
                        status: item.item.status,
                        communication: item.item,
                      })
                    }>
                    <Card1>
                      <View style={styles.card}>
                        <Image
                          style={styles.image}
                          source={{uri: item.item.imgUrl}}
                        />
                        <View style={styles.view}>
                          <View style={styles.textView}>
                            <Text style={styles.dateText}>
                              {item.item.date}
                            </Text>
                            <Text style={styles.text}>{item.item.title}</Text>
                          </View>
                          <View style={styles.iconsView}>
                            <TouchableOpacity
                              onPress={this.removePost.bind(
                                this,
                                item.item._id,
                              )}>
                              <Icon2
                                style={styles.icons}
                                name="md-archive"
                                color="green"
                                size={23}
                              />
                            </TouchableOpacity>
                            <TouchableOpacity
                              onPress={() => this.selectItem1(item)}>
                              <Icon
                                style={[
                                  styles.selected,
                                  item.item.selectedClass1,
                                ]}
                                name="heart"
                                color={this.state.color1}
                                size={22}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    </Card1>
                  </TouchableOpacity>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 4,
    backgroundColor: 'black',
    width: width,
  },

  vieww: {
    flexGrow: 4,

    width: width,
    height: height - 170,
  },
  imagecomunication: {
    height: height,
    width: width,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    flex: 0,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 40,
  },
  image: {
    width: width / 2.7,
    height: height / 7,
    borderRadius: 15,
    marginLeft: 10,
    position: 'relative',
    opacity: 0.7,
  },
  view: {
    flexDirection: 'column',
    height: height / 7,
    width: width / 1.78,
  },
  textView: {
    flexDirection: 'column',
    alignItems: 'stretch',
    marginTop: 10,
    justifyContent: 'flex-start',
    fontFamily: 'verdana',

    height: height / 13,
  },
  dateText: {
    fontSize: 11,
    fontFamily: 'verdana',
    color: 'grey',
    marginLeft: 10,
  },
  text: {
    fontSize: 15,
    color: 'white',
    fontFamily: 'verdana',
    marginLeft: 10,
    fontWeight: 'bold',
  },
  icons: {
    marginHorizontal: 7,
    color: 'green',
  },
  icons2: {
    marginHorizontal: 7,
  },
  iconsView: {
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'flex-end',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    marginTop: 5,
  },
  textInput: {
    fontFamily: 'verdana',
    fontSize: 16,
    marginLeft: 10,
    color: 'white',
  },
  list: {
    paddingVertical: 0,
    margin: 0,
    flexDirection: 'row',
    backgroundColor: 'black',
    justifyContent: 'flex-start',
    alignItems: 'center',
    zIndex: -1,
    color: 'white',
    marginHorizontal: 7,
  },
  selected: {
    color: 'green',
    marginHorizontal: 7,
  },
  viewSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    width: 350,
  },
  icons1: {
    marginLeft: 20,
  },
});
