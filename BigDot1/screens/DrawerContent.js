/* eslint-disable prettier/prettier */
import React from 'react';
import { View, StyleSheet, StatusBar } from 'react-native';
import {
    Avatar,
    Title,
    Caption,
    Dimensions,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
    shadow,
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem,
} from '@react-navigation/drawer';
import Users1 from '../model/user1';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
Icon.loadFont();

import { AuthContext } from '../components/context';
import PratictionerTabScreen from './PratictionerTabScreen';
import ApproverTabScreen from './ApproverTabScreen';
import { HeaderHeightContext } from '@react-navigation/stack';

export function DrawerContent(props) {
   
    const [isApprover, setIsApprover] = React.useState(false);
    const [Pratictioner, isTrue] = React.useState(false);
    
    const { signOut } = React.useContext(AuthContext);

    const _onToggleSwitch = () => {
        setIsApprover(!isApprover);
        isTrue(!Pratictioner)
    }

    return (
        <View style={{flex:1, backgroundColor:'#86BC25'}}>
            <StatusBar
                    backgroundColor="black"
                    barStyle="light-content"
            />
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'column',marginTop: 15,justifyContent: 'center', alignItems: 'center'}}>
                            <Avatar.Image
                               source={{uri: 'https://media-exp1.licdn.com/dms/image/C4D03AQFWqwF0UCoR0Q/profile-displayphoto-shrink_800_800/0?e=1600300800&v=beta&t=X_c7RvEJ4gytlQ08tVyosZqR9HnvFBUIfd9fqml96I4'}
                            }
                                size={50}
                            />
                            <View style={{ flexDirection:'column',justifyContent: 'center', alignItems: 'center'}}>
                                <Title style={styles.title}>Alessandro Mercuri</Title>
                                {/*<Caption style={styles.caption}>@deloitte.it</Caption>*/}
                            </View>
                        </View>
                        <View style={{justifyContent: 'center', alignItems: 'center'}}>
                            <Title style={styles.caption}>Deloitte Central Mediterranean</Title>
                            <Title style={styles.caption}>Consulting Leader</Title>
                        </View>
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={({color, size}) => (
                                <Icon
                                name="home-outline"
                                color={color}
                                size={size}
                                />
                            )}
                            label="Home"
                            onPress={() => {props.navigation.navigate('Home');}}
                        />


                        {Users1.username ? null : (
                        <DrawerItem
                            icon={({color, size}) => (
                                <Icon
                                name="account-outline"
                                color={color}
                                size={size}
                                />
                            )}
                            label="Approver Profile"
                            onPress={() => {props.navigation.navigate('Approver');}}
                        />
                        )}

                        {/*<DrawerItem
                            icon={({color, size}) => (
                                <Icon
                                name="settings-outline"
                                color={color}
                                size={size}
                                />
                            )}
                            label="Settings"
                            onPress={() => {props.navigation.navigate('Settings');}}
                        />*/}
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                    icon={({color, size}) => (
                        <Icon
                        name="exit-to-app"
                        color={color}
                        size={size}
                        />
                    )}
                    label="Sign Out"
                    onPress={() => {signOut();}}
                />
            </Drawer.Section>
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {

    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    title1: {
        fontSize: 12,
        marginTop: 3,
      },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 25,
   
    },
    bottomDrawerSection: {
        marginBottom: 15,
        position:'absolute',
        marginTop:450,
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });
