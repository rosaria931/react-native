/* eslint-disable prettier/prettier */
import React from 'react';
import {Button,Image, StyleSheet} from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import ToApproveScreen from './ToApproveScreen';
import PendingScreen2 from './PendingScreen2';
import DeniedScreen2 from './DeniedScreen2';
import ApprovedScreen2 from './ApprovedScreen2';
import BellsApproverScreen from './BellsApproverScreen';


import Icon from 'react-native-vector-icons/Ionicons';
import {DrawerContent} from './DrawerContent';
Icon.loadFont();
const Tab1 = createMaterialBottomTabNavigator();

const Tab = createMaterialTopTabNavigator();
const ApproverStack = createStackNavigator();


const ApproverTabScreen = () => (
  <Tab.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: 'green',
      inactiveTintColor: 'white',
      showIcon: 'true',
      pressColor: 'green',
      showLabel: false,
      labelStyle: { fontSize: 0 },
      style: { backgroundColor: 'black'},
    }}
  >
    <Tab.Screen
      name="ToApproveScreen"
      component={ToApproveScreen}
      options={{
        tabBarLabel: '',
        tabBarColor: 'black',
        tabBarIcon: ({ color }) => (
          <Icon name="ios-more" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="PendingScreen"
      component={PendingScreen2}
      options={{
        tabBarLabel: '',
        tabBarColor: 'black',
        tabBarIcon: ({ color }) => (
          <Icon name="md-trending-up" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="DeniedScreen2"
      component={DeniedScreen2}
      options={{
        tabBarLabel: '',
        tabBarColor: 'black',
        tabBarIcon: ({ color }) => (
          <Icon name="md-close" color={color} size={27} />
        ),
      }}
    />
    <Tab.Screen
      name="ApprovedScreen2"
      component={ApprovedScreen2}
      options={{
        tabBarLabel: '',
        tabBarColor: 'black',
        tabBarIcon: ({ color }) => (
          <Icon name="md-checkmark" color={color} size={27} />
        ),
      }}
    />
    <Tab.Screen
      name="NotificationApprover"
      component={BellsApproverScreen}
      options={{
        tabBarLabel: '',
        tabBarColor: 'black',
        tabBarIcon: ({ color }) => (
          <Icon name="ios-notifications" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default ApproverTabScreen;

const Logo = require('../assets/Deloittewhite.png');

// const per lo Stack di Approver
const ApproverStackScreen = ({navigation}) => (
<ApproverStack.Navigator
  screenOptions={{
    headerStyle: {
      backgroundColor: 'black',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  }}>
  <ApproverStack.Screen
    name=" "
    component={PendingScreen2}
    options={{
      headerLeft:() => (
        <Image resizeMode= "contain" source={Logo} style={styles.Logo} />
      ),
      headerRight: () => (
        <Icon.Button
          image={Logo}
          name="ios-menu"
          size={25}
          backgroundColor="black"
          onPress={() => {
            navigation.openDrawer();
          }}
        />
      ),
    }}
  />
</ApproverStack.Navigator>
);

const styles = StyleSheet.create({
Logo: {
  width: 145,
  height: 40,
},
});

