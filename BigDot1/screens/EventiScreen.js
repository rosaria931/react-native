/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');

export default class ConsultingLeaderScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      fullData: [],
      loading: false,
      error: null,
      query: '',
      selectCatg: 0,
    };
  }
  _onRefresh() {
    this.setState({loading: true});
    this.getDataFromAPI();
  }
  componentDidMount() {
    this.getDataFromAPI();
  }
  getDataFromAPI = _.debounce(() => {
    this.setState({loading: true});
    const apiURL = //'http://localhost:5000/';
    'https://9e564052c901.ngrok.io/';
    fetch(apiURL)
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          loading: false,
          data: resJson,
          fullData: resJson,
        });
      })
      .catch(error => {
        this.setState({error, loading: false});
      });
  }, 250);

  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 210,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  _renderItem(item) {
    let note = item.body;
    let statusComunication = item.status;
    let catg1 = item.categorie;
    if (catg1 === 4 && statusComunication === 5 && note === null) {
      return (
        <View>
          <TouchableOpacity
            transparent
            onPress={() =>
              this.props.navigation.navigate('ComunicationScreen2', {
                id: item._id,
                title: item.title,
                data: item.date,
                img: item.imgUrl,
                pdf: item.pdfUrl,
                communication: item,
              })
            }>
            <Card1>
              <View style={styles.card}>
                <Image style={styles.image} source={{uri: item.imgUrl}} />
                <View style={styles.view}>
                  <View style={styles.textView}>
                    <Text style={styles.dateText}>{item.date}</Text>
                    <Text style={styles.text}>{item.title}</Text>
                  </View>
                  <View style={styles.iconsView}>
                    <TouchableOpacity onPress={() => alert('')}>
                      <Icon2
                        style={styles.icons}
                        name="md-archive"
                        color="green"
                        size={23}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => alert('')}>
                      <Icon
                        style={styles.icons}
                        name="heart"
                        color="green"
                        size={22}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Card1>
          </TouchableOpacity>
        </View>
      );
    }
  }

  handleSearch = text => {
    const formattedQuery = text.toUpperCase();
    const data = _.filter(this.state.fullData, search => {
      if (search.title.toUpperCase().indexOf(formattedQuery) > -1) {
        return true;
      }
      return false;
    });
    this.setState({data, query: text});
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewSearch}>
          <View style={styles.return1}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon2
                style={styles.icons2}
                name="ios-arrow-dropleft"
                color="grey"
                size={35}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.viewsearch1}>
            <TextInput
              style={styles.textInput}
              placeholder="Search"
              onChangeText={this.handleSearch}
            />
          </View>
          <Icon2
            style={styles.icons1}
            name="ios-search"
            color="grey"
            size={23}
          />
        </View>
        <Text style={styles.news}> Events</Text>
        <View style={styles.one}>
          <FlatList
            data={this.state.data}
            renderItem={({item}) => this._renderItem(item)}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={this.renderFooter}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh.bind(this)}
                tintColor="gray"
                title="loading..."
              />
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 4,
    backgroundColor: 'black',
  },
  one: {
    height: height,
  },
  news: {
    color: 'white',
    fontFamily: 'verdana',
    fontSize: 16,
    fontWeight: 'bold',
    height: height / 30,
    width: width,

    marginLeft: 17,
    justifyContent: 'center',
  },
  imagecomunication: {
    height: height,
    width: width,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    flex: 0,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 40,
  },
  image: {
    width: width / 2.7,
    height: height / 7,
    borderRadius: 15,
    marginLeft: 10,
    position: 'relative',
    opacity: 0.7,
    borderWidth: 3,
    borderColor: 'green',
  },
  view: {
    flexDirection: 'column',
    height: height / 7,
    width: width / 1.78,
  },
  textView: {
    flexDirection: 'column',
    alignItems: 'stretch',
    marginTop: 8,
    justifyContent: 'flex-start',
    fontFamily: 'verdana',

    height: height / 11,
  },
  dateText: {
    fontSize: 11,
    fontFamily: 'verdana',
    color: 'grey',
    marginLeft: 10,
  },
  text: {
    fontSize: 15,
    color: 'white',
    fontFamily: 'verdana',
    marginLeft: 10,
    fontWeight: 'bold',
  },
  icons: {
    marginHorizontal: 7,
    color: 'green',
  },
  viewsearch1: {
    flexDirection: 'row',
  },
  iconsView: {
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'flex-end',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    height: height / 30,
  },
  textInput: {
    fontFamily: 'verdana',
    fontSize: 13,
    color: 'white',
    width: width / 4.5,
    marginLeft: 60,
    paddingLeft: 10,
  },
  viewSearch: {
    flexDirection: 'row',
    height: height / 18,
    width: width,
    alignContent: 'flex-end',
  },
  icons2: {
    marginLeft: 10,
  },
  icons1: {
    marginTop: 5,
  },
  return1: {
    justifyContent: 'flex-start',
    width: width / 1.9,
  },
});
