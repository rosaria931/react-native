/* eslint-disable prettier/prettier */
import React from 'react';
import {Button, Image, StyleSheet, Dimensions} from 'react-native';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createStackNavigator} from '@react-navigation/stack';

import BlackboardScreen5 from './BlackboardScreen5';
import SaveScreen5 from './SaveScreen5';
import TagScreen2 from './TagScreen2';
import BellsScreen from './BellsScreen';

import Icon from 'react-native-vector-icons/Ionicons';
Icon.loadFont();
import {DrawerContent} from './DrawerContent';

const PratictionerStack = createStackNavigator();

const Tab = createMaterialTopTabNavigator();

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const PratictionerTabScreen = navigation => (
  <Tab.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      height: windowHeight,
      activeTintColor: '#86BC25',
      inactiveTintColor: 'white',
      showIcon: 'true',
      pressColor: '#86BC25',
      showLabel: false,
      labelStyle: {fontSize: 0},
      style: {backgroundColor: 'black'},
    }}>
    <Tab.Screen
      name="BlackBoard"
      component={BlackboardScreen5}
      options={{
        tabBarLabel: 'Blackboard',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="ios-home" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Salvati"
      component={SaveScreen5}
      options={{
        tabBarLabel: '',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="md-archive" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Categorie"
      component={TagScreen2}
      options={{
        tabBarLabel: '',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="md-pricetags" color={color} size={26} />
        ),
      }}
    />
    <Tab.Screen
      name="Notification"
      component={BellsScreen}
      options={{
        tabBarLabel: '',
        tabBarColor: '#009387',
        tabBarIcon: ({color}) => (
          <Icon name="ios-notifications" color={color} size={26} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default PratictionerTabScreen;

const styles = StyleSheet.create({
  Logo: {
    width: 145,
    height: 40,
  },
});
