/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  RefreshControl,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
import level1 from '../model/level';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');

export default class ApprovedScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      fullData: [],
      loading: false,
      error: null,
      query: '',
    };
  }
  _onRefresh() {
    this.setState({loading: true});
    this.getDataFromAPI();
  }

  componentDidMount() {
    this.getDataFromAPI();
  }
  getDataFromAPI = _.debounce(() => {
    this.setState({loading: true});
    const apiURL = 'https://9e564052c901.ngrok.io/';
      //'http://localhost:5000/';
    fetch(apiURL)
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          loading: false,
          data: resJson,
          fullData: resJson,
        });
      })
      .catch(error => {
        this.setState({error, loading: false});
      });
  }, 250);

  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 210,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  _renderItem = ({item, index}) => {
    //let owner1 = 'L1 Leader';
    //let owner = item.owner;
    let level = level1;
    let statusComunication = item.status;
    let note = item.body;
    if (statusComunication >= 5 && note === null) {
      return (
        <View>
          <TouchableOpacity
            transparent
            onPress={() =>
              this.props.navigation.navigate('ApprovedPreviewScreen', {
                id: item._id,
                title: item.title,
                data: item.date,
                img: item.imgUrl,
                pdf: item.pdfUrl,
                body: item.body,
                status: item.status,
                communication: item,
              })
            }>
            <Card1>
              <View style={styles.card}>
                <Image style={styles.image} source={{uri: item.imgUrl}} />
                <View style={styles.view}>
                  <View style={styles.textView}>
                    <Text style={styles.dateText}>{item.date}</Text>
                    <Text style={styles.text}>{item.title}</Text>
                    <Text style={styles.text}>{item.body}</Text>
                  </View>
                  <View style={styles.iconsView}>
                    <TouchableOpacity
                      transparent
                      onPress={() =>
                        this.props.navigation.navigate('ApproverStatusScreen', {
                          id: item._id,
                          title: item.title,
                          data: item.date,
                          img: item.imgUrl,
                          pdf: item.pdfUrl,
                          body: item.body,
                          status: item.status,
                          communication: item,
                        })
                      }>
                      <Text style={styles.icons}>Approved</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Card1>
          </TouchableOpacity>
        </View>
      );
    }
  };
  handleSearch = text => {
    const formattedQuery = text.toUpperCase();
    const data = _.filter(this.state.fullData, search => {
      if (search.title.toUpperCase().indexOf(formattedQuery) > -1) {
        return true;
      }
      return false;
    });
    this.setState({data, query: text});
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewSearch}>
          <Text style={styles.pending}>Approved</Text>
          <View style={styles.internalsearch}>
            <TextInput
              style={styles.textInput}
              placeholder="Search"
              onChangeText={this.handleSearch}
            />
            <Icon2
              style={styles.icons1}
              name="ios-search"
              color="grey"
              size={25}
            />
          </View>
        </View>
        <View style={{height: height/1.33}}>
          <FlatList
            data={this.state.data}
            renderItem={this._renderItem}
            keyExtractor={item => item._id.toString()}
            ListFooterComponent={this.renderFooter}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this._onRefresh.bind(this)}
                tintColor="gray"
                title="loading..."
              />
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'black',
  },
  imagecomunication: {
    height: height,
    width: width,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    flex: 1,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',

    shadowColor: 'black',
    shadowRadius: 4,
    resizeMode: 'contain',
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    justifyContent: 'flex-start',
    alignItems: 'center',
    elevation: 40,
  },
  image: {
    width: width / 2.7,
    height: height / 7,
    borderRadius: 15,
    marginLeft: 10,
    position: 'relative',
    opacity: 0.7,
  },
  textView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    fontFamily: 'verdana',
    width: width / 2,
    height: height / 15,
    position: 'relative',
    alignItems: 'stretch',
  },
  dateText: {
    fontSize: 11,
    fontFamily: 'verdana',
    color: 'grey',
    marginLeft: 10,
  },
  text: {
    fontSize: 15,
    color: 'white',
    height: height / 7,
    fontFamily: 'verdana',
    marginLeft: 10,
    fontWeight: 'bold',
  },
  icons: {
    color: 'green',
    marginHorizontal: 7,
    fontStyle: 'italic',
    fontFamily: 'verdana',
  },
  iconsView: {
    flexDirection: 'row',
    height: height / 30,
    position: 'relative',
    justifyContent: 'flex-end',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    width: width / 1.7,
    fontStyle: 'italic',
    marginRight: 7,
  },
  textInput: {
    fontFamily: 'verdana',

    fontSize: 10,
    color: 'white',
    width: width / 4.5,
    marginLeft: 60,
    paddingLeft: 10,
  },

  icons1: {
    marginHorizontal: 17,
    alignItems: 'center',
    marginTop: 3.5,
    fontStyle: 'italic',
    marginLeft: -6,
  },
  viewSearch: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: height / 19,
    width: width,
  },

  pending: {
    color: 'white',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    fontSize: 16,
    alignItems: 'flex-start',
    marginTop: 6,
  },
  internalsearch: {
    flexDirection: 'row',
    width: width / 1.35,
    justifyContent: 'flex-end',
    height: height / 19,
  },
  view: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
});
