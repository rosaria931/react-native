/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  RefreshControl,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Modal,
  Alert,
  ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import Card1 from '../components/Card1';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
import level1 from '../model/level';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');

export default class PendingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      fullData: [],
      loading: false,
      error: null,
      query: '',
      text: '',
      modalVisible: false,
    };
  }
  _onRefresh() {
    this.setState({loading: true});
    this.getDataFromAPI();}

  componentDidMount() {
    this.getDataFromAPI();
  }
  getDataFromAPI = _.debounce(() => {
    this.setState({loading: true});
    const apiURL = //  'http://localhost:5000/';
    'https://9e564052c901.ngrok.io/';
    fetch(apiURL)
      .then(res => res.json())
      .then(resJson => {
        this.setState({
          loading: false,
          data: resJson,
          fullData: resJson,
        });
      })
      .catch(error => {
        this.setState({error, loading: false});
      });
  }, 250);

  setModalVisible = visible => {
    this.setState({modalVisible: visible});
  };

  renderFooter = () => {
    if (!this.state.loading) {
      return null;
    }
    return (
      <View
        style={{
          paddingVertical: 40,
          borderTopWidth: 1,
          borderColor: '#CED0CE',
        }}>
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  handleSearch = text => {
    const formattedQuery = text.toUpperCase();
    const data = _.filter(this.state.fullData, search => {
      if (search.title.toUpperCase().indexOf(formattedQuery) > -1) {
        return true;
      }
      return false;
    });
    this.setState({data, query: text});
  };
  _renderItem1 = ({item, index}) => {
    const {modalVisible} = this.state;
    let level = level1;
    let note = item.body;
    let statusComunication = item.status;
    if (statusComunication > level && statusComunication < 5 && note === null) {
    return (
      <View>
        <TouchableOpacity
          transparent
          onPress={() => this.props.navigation.navigate('PendingPreviewScreen', {
            id: item._id,
            title: item.title,
            data: item.date,
            img: item.imgUrl,
            pdf: item.pdfUrl,
            communication: item
          })
        }>
          <Card1>
            <View style={styles.card1}>
              <Image style={styles.image} source={{uri: item.imgUrl}} />
              <View style={styles.view}>
                <View style={styles.textView}>
                  <Text style={styles.dateText}>{item.date}</Text>
                  <Text style={styles.text}>{item.title}</Text>
                  <Text style={styles.text}>{item.body}</Text>
                </View>
                <View style={styles.iconsView}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('PendingStatusScreen', {
                        id: item._id,
                        title: item.title,
                        data: item.date,
                        img: item.imgUrl,
                        pdf: item.pdfUrl,
                        status: item.status,
                        communication: item,
                      })
                    }>
                    <Text style={styles.icons}>Pending</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Card1>
        </TouchableOpacity>
      </View>
    
      
    );
  };
}
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.viewSearch}>
          <Text style={styles.pending}>Pending</Text>
          <View style={styles.internalsearch}>
            <TextInput
              style={styles.textInput}
              placeholder="Search"
              onChangeText={this.handleSearch}
            />
            <Icon2
              style={styles.icons1}
              name="ios-search"
              color="grey"
              size={25}
            />
          </View>
        </View>
        <View style={styles.container}>
          <View style={{height: (height - 85) / 1.2}}>
            <FlatList
              data={this.state.data}
              paddingBottom={160}
              renderItem={this._renderItem1}
              keyExtractor={item => item._id.toString()}
              ListFooterComponent={this.renderFooter}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.loading}
                  onRefresh={this._onRefresh.bind(this)}
                  tintColor = "gray"
                  title = "loading..." 
                />
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'black',
  },
  pending: {
    color: 'white',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    fontSize: 16,
    alignItems: 'flex-start',
    marginTop: 6,
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    flexDirection: 'row',
    flex: 1,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    resizeMode: 'contain',
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    justifyContent: 'flex-start',
    alignItems: 'center',
    elevation: 40,
  },
  iconsother: {
    marginTop: 10,
    alignItems: 'center',
  },
  card1: {
    flexDirection: 'row',
    flex: 0,
    width: width,
    height: height / 7,
    backgroundColor: '#0a0a0a',
    shadowColor: 'black',
    shadowRadius: 4,
    resizeMode: 'contain',
    shadowOffset: {width: 10, height: 2},
    shadowOpacity: 2,
    marginBottom: 1,
    marginTop: 6,
    justifyContent: 'flex-start',
    alignItems: 'center',
    elevation: 40,
  },
  image: {
    width: width / 2.7,
    height: height / 7,
    borderRadius: 15,
    marginLeft: 10,
    position: 'relative',
    opacity: 0.7,
  },
  view: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
  textView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    fontFamily: 'verdana',
    width: width / 2,
    height: height / 15,
    position: 'relative',
    alignItems: 'stretch',
  },
  dateText: {
    fontSize: 11,
    fontFamily: 'verdana',
    color: 'grey',
    marginLeft: 10,
  },
  text: {
    fontSize: 15,
    color: 'white',
    height: height / 7,
    fontFamily: 'verdana',
    marginLeft: 10,
    fontWeight: 'bold',
  },
  icons: {
    color: 'yellow',
    marginHorizontal: 7,
    fontStyle: 'italic',
    fontFamily: 'verdana',
  },
  iconsView: {
    flexDirection: 'row',
    height: height / 30,
    position: 'relative',
    justifyContent: 'flex-end',

    fontFamily: 'verdana',
    fontWeight: 'bold',
    width: width / 1.7,
    fontStyle: 'italic',
    marginRight: 7,
  },
  textInput: {
    fontFamily: 'verdana',
    fontSize: 13,
    color: 'white',
    width: width / 4.5,
    marginLeft: 60,
    paddingLeft: 10,
  },
  viewSearch: {
    flexDirection: 'row',

    justifyContent: 'flex-end',
    height: height / 19,
    width: width,
    alignItems: 'center',
  },
  icons1: {
    marginHorizontal: 17,
    alignItems: 'center',
    marginLeft: -6,
    marginTop: 3.5,
    fontStyle: 'italic',
  },
  centeredView: {
    flex: 1,
  },
  modalView: {
    flexGrow: 4,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalText: {
    fontSize: 32,
    color: 'green',
  },
  internalsearch: {
    flexDirection: 'row',
    width: width / 1.3,
    justifyContent: 'flex-end',
    height: height / 18,
  },
});
