/* eslint-disable prettier/prettier */
import React, {Component} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Pdf from 'react-native-pdf';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
RefreshControl,
  ScrollView,
  Animated,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,
  TouchableHighlight,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';

Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');
const scale = new Animated.Value(1);
export default class ComunicationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      imageComunication: '',
      like: false,
      loading: false,
      color1: this.color(),
      color2: this.color1(),
      save: false,
      id: [],
    };
  }
  static defaultProps = {
    doAnimateZoomReset: false,
    maximumZoomScale: 10,
    minimumZoomScale: 1,
    zoomHeight: height,
    zoomWidth: width,
  };

  handleResetZoomScale = event => {
    this.scrollResponderRef.scrollResponderZoomTo({
      x: 0,
      y: 0,
      width: this.props.zoomWidth,
      height: this.props.zoomHeight,
      animated: true,
    });
  };

  setZoomRef = node => {
    // ScrollView ha un scrollResponder che ci consente di accedere a più metodi per controllare il componente ScrollView
    if (node) {
      this.zoomRef = node;
      this.scrollResponderRef = this.zoomRef.getScrollResponder();
    }
  };

  _onRefresh() {
    this.setState({loading: true});
    this.color();
    this.color1();
  }
  color () {
    AsyncStorage.getItem('card')
    .then(card => {
     
      if (card !== null) {
        const card1 = JSON.parse(card);
        

          if (card.includes(this.props.route.params.communication._id) === true){
            this.setState({color1:"green"})
            this.setState({loading: false});
          } else {
            this.setState({color1:"grey"})
            this.setState({loading: false});
           }
          
        } else {
          this.setState({color1:"grey"})}
          this.setState({loading: false});
          });
        
      };

      color1 () {
        AsyncStorage.getItem('like')
        .then(card1 => {
         
          if (card1 !== null) {
            const card2 = JSON.parse(card1);
            
    
              if (card1.includes(this.props.route.params.communication._id) === true){
                this.setState({color2:"green"})
                this.setState({loading: false});
              } else {
                this.setState({color2:"grey"})
                this.setState({loading: false});
               }
              
            } else {
              this.setState({color2:"grey"})}
              this.setState({loading: false});
              });
            
          };


selectItem = items => {

    AsyncStorage.getItem('card')
    .then(card => {
      if (card !== null) {
           const card1 = JSON.parse(card);
          if (card.includes(this.props.route.params.communication._id) !== true){     
           
            card1.push(this.props.route.params.communication);
           
            AsyncStorage.setItem('card', JSON.stringify(card1));
            
           alert('Added to Favorites');
           this.setState({color1:"green"})
          
          } else {
            
            const postsItems = card1.filter(function(e){return e.id !== items._id });
                    
          
            AsyncStorage.setItem('card', JSON.stringify(postsItems));
           alert("Removed from Fravorites")
            this.setState({color1:"grey"})
           
        }
      } else {
  
        AsyncStorage.clear();
        const card = [];
        card.push(items);
        AsyncStorage.setItem('card', JSON.stringify(card));
       
        alert('Added to Favourites');
        this.setState({color1:"green"})
      }
      
    })
    .catch(err => {
      alert(err);
    });
  };
  selectItem1 = items => {

    AsyncStorage.getItem('like')
    .then(card1 => {
      if (card1 !== null) {
           const card2 = JSON.parse(card1);
          if (card1.includes(this.props.route.params.communication._id) !== true){     
           
            card2.push(this.props.route.params.communication);
           
            AsyncStorage.setItem('like', JSON.stringify(card2));
            
           alert('I like it! ');
           this.setState({color2:"green"})
          
          } else {
            
            const postsItems = card2.filter(function(e){return e.id !== items._id });
                    
          
            AsyncStorage.setItem('like', JSON.stringify(postsItems));
           alert("Removed like :(")
            this.setState({color2:"grey"})
           
        }
      } else {
  
        AsyncStorage.clear();
        const card1 = [];
        card1.push(items);
        AsyncStorage.setItem('like', JSON.stringify(card1));
       
        alert('I like it! ');
        this.setState({color2:"green"})
      }
      
    })
    .catch(err => {
      alert(err);
    });
  };
  componentDidMount() {
    //this.getDataFromAPI();
    //console.log("pdfUrl: " + JSON.stringify(this.props.route.params.communication.imgUrl));
  }
  /*
   = async () => {
    const endpoint = //'https://64c2f1d6e936.ngrok.io/posts';
      'http://localhost:5000';
    const res = await fetch(endpoint);
    const data = await res.json();

    this.setState({items: data});

    {
    }
  };*/
  /*
  _renderItem = ({item, index}) => {
    //const {id} = this.props.route.params;
    //const {title} = this.props.route.params;
    //const {data} = this.props.route.params;
    //const {img} = this.props.route.params;
    //const {cat} = this.props.route.params;
    //const {pdf} = this.props.route.params;

    const source = {uri:'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',cache:true};

    return (
      <View style={styles.container}>
        <View style={styles.return}>
          <View style={styles.return1}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon2
                style={styles.icons1}
                name="ios-arrow-dropleft"
                color="grey"
                size={35}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.icons}>
            <TouchableOpacity onPress={() => this.save()}>
              <Icon2
                style={styles.icons1}
                name="md-archive"
                color={this.state.color1}
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.like()}>
              <Icon2
                style={styles.icons1}
                name="md-heart"
                color={this.state.color2}
                size={30}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.text}>
          <Text style={styles.date}>Italy | Consulting | {this.props.route.params.communication.data}</Text>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>
            An initiative by Alessandro Mercuri
          </Text>
        </View>

        <ScrollView
          contentContainerStyle={styles.main}
          maximumZoomScale={this.props.maximumZoomScale}
          minimumZoomScale={this.props.minimumZoomScale}
          showsHorizontalScrollIndicator={true}
          showsVerticalScrollIndicator={false}
          ref={this.setZoomRef}>

          <TouchableHighlight onPress={this.handleResetZoomScale}>
            <Pdf
                    source={source}
                    onLoadComplete={(numberOfPages,filePath)=>{
                        console.log(`number of pages: ${numberOfPages}`);
                    }}
                    onPageChanged={(page,numberOfPages)=>{
                        console.log(`current page: ${page}`);
                    }}
                    onError={(error)=>{
                        console.log(error);
                    }}
                    onPressLink={(uri)=>{
                        console.log(`Link presse: ${uri}`)
                    }}
                    style={styles.pdf}/>
          </TouchableHighlight>


        </ScrollView>
        <View style={styles.footerview}>
          <Text style={styles.footer}>
            Questa comunicazione è destinata a esclusiva condivisione interna e
            può essere utilizzata solamente dalle persone di Deloitte Touche
            Tohmatsu Limited, dalle sue member firm e delle legal entity che le
            compongono (definite nel complesso “network Deloitte”). Nessuno dei
            network Deloitte è responsabile di qualsiasi perdita sostenuta da
            chiunque faccia affidamento sulla presente comunicazione.
          </Text>
          <Text style={styles.footer}>
            Il nome Deloitte si riferisce a una o più delle seguenti entità:
            Deloitte Touche Tohmatsu Limited, una società inglese a
            responsabilità limitata (“DTTL”), le member firm aderenti al suo
            network e le entità a esse correlate. DTTL e ciascuna delle sue
            member firm sono entità giuridicamente separate e indipendenti tra
            loro. DTTL (denominata anche “Deloitte Global”) non fornisce servizi
            ai clienti. Si invita a leggere l’informativa completa relativa alla
            descrizione della struttura legale di Deloitte Touche Tohmatsu
            Limited e delle sue member firm all’indirizzo
            www.deloitte.com/about.
          </Text>

          <Text style={styles.footer}>© Deloitte Consulting S.r.l.</Text>
        </View>
      </View>
    );
  };*/

  render() {


   
    const source = {uri:this.props.route.params.communication.pdfUrl,cache:true};
    let {items} = this.state;
   
    if (this.props.route.params.communication === undefined) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.container}
     >
        <View style={styles.return}>
          <View style={styles.return1}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon2
                style={styles.icons1}
                name="ios-arrow-dropleft"
                color="grey"
                size={35}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.icons}>
            <TouchableOpacity onPress={() => this.selectItem(items)}>
              <Icon2
                style={styles.icons1}
                name="md-archive"
                color={this.state.color1}
                size={30}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.selectItem1(items)}>
              <Icon2
                style={styles.icons1}
                name="md-heart"
                color={this.state.color2}
                size={30}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.text}>
          <Text style={styles.date}>Italy | Consulting | {this.props.route.params.communication.date}</Text>
          <Text style={styles.title}>{this.props.route.params.communication.title}</Text>
          <Text style={styles.subtitle1}>
          <Text style={styles.subtitle}>
            An initiative by 
          </Text>
          <Text>  </Text>
          <Text style={styles.subtitle2}>L1 Leader</Text>
          </Text>
        </View>

        <ScrollView
          contentContainerStyle={styles.main}
          maximumZoomScale={this.props.maximumZoomScale}
          minimumZoomScale={this.props.minimumZoomScale}
          showsHorizontalScrollIndicator={true}
          showsVerticalScrollIndicator={false}
          ref={this.setZoomRef}
          refreshControl={
            <RefreshControl
              refreshing={this.state.loading}
              onRefresh={this._onRefresh.bind(this)}
              tintColor = "gray"
              title = "loading..." 
            />
          }>

          <TouchableHighlight onPress={this.handleResetZoomScale}>
            <Pdf
                    source={source}
                    style={styles.pdf}/>
          </TouchableHighlight>


        </ScrollView>
       {/* <View style={styles.footerview}>
          <Text style={styles.footer}>
            Questa comunicazione è destinata a esclusiva condivisione interna e
            può essere utilizzata solamente dalle persone di Deloitte Touche
            Tohmatsu Limited, dalle sue member firm e delle legal entity che le
            compongono (definite nel complesso “network Deloitte”). Nessuno dei
            network Deloitte è responsabile di qualsiasi perdita sostenuta da
            chiunque faccia affidamento sulla presente comunicazione.
          </Text>
          <Text style={styles.footer}>
            Il nome Deloitte si riferisce a una o più delle seguenti entità:
            Deloitte Touche Tohmatsu Limited, una società inglese a
            responsabilità limitata (“DTTL”), le member firm aderenti al suo
            network e le entità a esse correlate. DTTL e ciascuna delle sue
            member firm sono entità giuridicamente separate e indipendenti tra
            loro. DTTL (denominata anche “Deloitte Global”) non fornisce servizi
            ai clienti. Si invita a leggere l’informativa completa relativa alla
            descrizione della struttura legale di Deloitte Touche Tohmatsu
            Limited e delle sue member firm all’indirizzo
            www.deloitte.com/about.
    </Text> */}
          <View>
          
       </View>
    </View>
    );
  
}}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    backgroundColor: 'black',
  },
  container2: {
    flex: 2,
  },
  footerview: {
    height: height / 7.5,
    resizeMode: 'center',
    width: width / 1.01,
    padding: 3,
  },
  footer: {
    color: 'white',
    fontFamily: 'verdana',
    fontSize: 6,
    textAlign: 'justify',
    padding: 5,
  },
  main: {
    flex: 1,
    height: height, //if communication==1 height*0.7
    resizeMode: 'center',
    position: 'absolute',
    justifyContent: 'center',
    marginTop: 0,
    backgroundColor: 'black',
    width: width,
    alignItems: 'center',
    flexDirection: 'column',
  },
  imagecomunication: {
    height: height,
    width: width,
    marginTop: 14, //and margin top 100
    paddingBottom: 50,
    position: 'relative',
    resizeMode: 'contain',
    justifyContent: 'center',
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  loader: {
    marginTop: 270,
    alignItems: 'center',
    justifyContent: 'center',
  },
  return: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    height: height / 17,
    width: width,
    alignItems: 'center',
  },
  return1: {
    justifyContent: 'flex-start',
    width: width / 1.4,
  },
  text: {
    paddingLeft: 10,
    padding: 5,
  },
  title: {
    color: 'white',
    fontFamily: 'verdana',
    marginTop: 7,
    fontSize: 19,
    fontWeight: 'bold',
  },
  icons: {
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'flex-end',
    marginRight: 12,
  },
  date: {
    color: 'grey',
    fontFamily: 'verdana',
    fontSize: 11,
    fontWeight: 'bold',
  },
  icons1: {
    marginHorizontal: 12,
  },
  subtitle: {
    color: 'white',
    fontFamily: 'verdana',
    marginTop: 3,
    fontSize: 15,
  },
  subtitle2: {
    color: 'green',
    fontFamily: 'verdana',
    marginTop: 3,
    fontSize: 15,
    marginLeft: 5,
   
  },
  subtitle1: {
   flexDirection:'row',
  
},
list: {
  paddingVertical: 0,
  margin: 0,
  flexDirection: 'row',
  backgroundColor: 'black',
  justifyContent: 'flex-start',
  alignItems: 'center',
  zIndex: -1,
  color: 'grey',
  marginHorizontal: 7,
},
selected: {
  color: 'green',
},
});
