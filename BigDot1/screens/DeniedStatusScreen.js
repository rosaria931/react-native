/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import MinMaxTextInput from '../components/MinMaxTextInput';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/Ionicons';
Icon.loadFont();
Icon1.loadFont();
Icon2.loadFont();
const {height, width} = Dimensions.get('window');

export default class StatusScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  displayComponent() {
    let status = this.props.route.params.communication.status;
    if (status === 1) {
      return (
        <View style={{backgroundColor: 'yellow'}}>
          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Richiesta</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>L1</Text>
                <Text style={styles.specifica}>ha inoltrato la richiesta </Text>
              </View>
            </View>
          </View>
        </View>
      );
    } else if (status === 2) {
      return (
        <View style={styles.container}>
          <View style={styles.container}>
            <View style={styles.riga}>
              <View style={styles.checkmark}>
                <Icon2
                  style={{}}
                  name="ios-checkmark"
                  color="white"
                  size={32}
                />
              </View>

              <View style={styles.testo1}>
                <View style={styles.fase}>
                  <Text style={styles.textfase}>Richiesta</Text>
                  <Text style={styles.data}>13 07 2020</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.nome}>L1</Text>
                  <Text style={styles.specifica}>
                    ha inoltrato la richiesta{' '}
                  </Text>
                </View>
              </View>
            </View>

            <View style={styles.riga}>
              <View style={styles.close}>
                <Icon2 style={{}} name="ios-close" color="white" size={32} />
              </View>

              <View style={styles.testo1}>
                <View style={styles.fase}>
                  <Text style={styles.textfase}>Approvazione Template</Text>
                  <Text style={styles.data}>13 07 2020</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={styles.nomeNegato}>Brand&Comunication</Text>
                  <Text style={styles.specifica}>
                    ha rigettato la richiesta{' '}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      );
    } else if (status === 3) {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Richiesta</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>L1</Text>
                <Text style={styles.specifica}>ha inoltrato la richiesta </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Approvazione Template</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>Brand&Comunication</Text>
                <Text style={styles.specifica}>ha approvato il Template </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.close}>
              <Icon2 style={{}} name="ios-close" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Prima Approvazione</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nomeNegato}>CoS Consulting Leader</Text>
                <Text style={styles.specifica}>ha rigettato la richiesta </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else if (status === 4) {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Richiesta</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>L1</Text>
                <Text style={styles.specifica}>ha inoltrato la richiesta </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Approvazione Template</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>Brand&Comunication</Text>
                <Text style={styles.specifica}>ha approvato il Template </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Prima Approvazione</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>CoS Consulting Leader</Text>
                <Text style={styles.specifica}>
                  ha approvato la comunicazione{' '}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.close}>
              <Icon2 style={{}} name="ios-close" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Seconda Approvazione</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nomeNegato}>Consulting Leader</Text>
                <Text style={styles.specifica}>ha rigettato la richiesta </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else if (status === 5) {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Richiesta</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>L1</Text>
                <Text style={styles.specifica}>ha inoltrato la richiesta </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Approvazione Template</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>Brand&Comunication</Text>
                <Text style={styles.specifica}>ha approvato il Template </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Prima Approvazione</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>CoS Consulting Leader</Text>
                <Text style={styles.specifica}>
                  ha approvato la comunicazione{' '}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Seconda Approvazione</Text>
                <Text style={styles.data}>13 07 2020 </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>Consulting Leader</Text>
                <Text style={styles.specifica}>
                  ha approvato la comunicazione{' '}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.riga1}>
            <View style={styles.close}>
              <Icon2 style={{}} name="ios-close" color="white" size={28} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Pubblicazione</Text>
                <Text style={styles.data}>Today</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nomeNegato}>CoS Consulting Leader</Text>
                <Text style={styles.specifica}>ha rigettato la richiesta </Text>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <View style={{backgroundColor: 'yellow'}}>
          <View style={styles.riga}>
            <View style={styles.checkmark}>
              <Icon2 style={{}} name="ios-checkmark" color="white" size={32} />
            </View>

            <View style={styles.testo1}>
              <View style={styles.fase}>
                <Text style={styles.textfase}>Approvazione Template</Text>
                <Text style={styles.data}>13 07 2020</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={styles.nome}>Brand&Comunication</Text>
                <Text style={styles.specifica}>ha approvato il Template </Text>
              </View>
            </View>
          </View>
        </View>
      );
    }
  }
  render() {
    let {items} = this.state;
    if (this.props.route.params.communication === undefined) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.viewSearch}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('DeniedScreen2')}>
            <Icon2
              style={styles.icons1}
              name="ios-arrow-dropleft"
              color="grey"
              size={35}
            />
          </TouchableOpacity>
          <View style={styles.state}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('DeniedPreviewScreen', {
                  id: this.props.route.params.communication._id,
                  title: this.props.route.params.communication.title,
                  data: this.props.route.params.communication.date,
                  img: this.props.route.params.communication.imgUrl,
                  pdf: this.props.route.params.communication.pdfUrl,
                  body: this.props.route.params.communication.body,
                  status: this.props.route.params.communication.status,
                  communication: this.props.route.params.communication,
                })
              }>
              {/*} <Icon2 style={styles.icons1} name="md-eye" color="green" size={35} label="Preview"/>*/}
              <Text style={styles.textInput}>Preview</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('DeniedStatusScreen', {
                  id: this.props.route.params.communication._id,
                  title: this.props.route.params.communication.title,
                  data: this.props.route.params.communication.date,
                  img: this.props.route.params.communication.imgUrl,
                  pdf: this.props.route.params.communication.pdfUrl,
                  body: this.props.route.params.communication.body,
                  status: this.props.route.params.communication.status,
                  communication: this.props.route.params.communication,
                })
              }>
              <Text style={styles.textInput1}>Status</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.view1}>
          <View style={{height: (height - 100) / 2}}>
            {this.displayComponent()}
          </View>

          <Text style={styles.nota}>Reason for the rejected request:</Text>
          <View
            style={{
              backgroundColor: '#0a0a0a',
              borderRadius: 15,
              height: height / 4,
              width: width / 1.1,
              marginTop: 10,
              padding: 10,
            }}>
            <Text style={styles.description}>
              {this.props.route.params.communication.body}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    height: (height - 50) / 4,
    width: width,
  },
  riga: {
    flexDirection: 'row',
    backgroundColor: 'black',
    padding: 7,
  },
  riga1: {
    flexDirection: 'row',
    backgroundColor: 'black',
    padding: 7,
    alignItems: 'center',
  },
  data: {
    color: 'grey',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    fontSize: 10,
    width: width / 1.8,
  },
  specifica: {
    color: 'white',
    marginTop: 3,
    marginLeft: 5,
    fontFamily: 'verdana',
    fontSize: 12,
  },

  nome: {
    color: 'green',
    marginTop: 3,
    marginLeft: 10,
    fontFamily: 'verdana',
    fontSize: 12,
  },
  nomeNegato: {
    color: 'red',
    marginTop: 3,
    marginLeft: 10,
    fontFamily: 'verdana',
    fontSize: 12,
  },

  checkmark: {
    backgroundColor: 'green',
    width: 30,
    height: 30,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2,
  },
  future: {
    backgroundColor: '#0a0a0a',
    width: 30,
    height: 30,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2,
  },
  pending: {
    backgroundColor: 'orange',
    width: 30,
    height: 30,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2,
  },
  textfase: {
    color: 'white',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    fontSize: 12,
  },

  textfase1: {
    color: '#0a0a0a',
    fontFamily: 'verdana',
    fontWeight: 'bold',
    fontSize: 12,
  },

  testo1: {
    flexDirection: 'column',
    fontFamily: 'verdana',
  },
  fase: {
    flexDirection: 'row',
    fontFamily: 'verdana',
    justifyContent: 'space-between',
    fontWeight: 'bold',
    marginLeft: 10,
    width: width,
  },

  icons1: {
    marginLeft: 10,
    marginHorizontal: 7,
  },
  view1: {
    height: height,
    backgroundColor: 'black',
    padding: 15,
  },
  approvationview: {
    flexDirection: 'row',
    width: width / 1.08,
    resizeMode: 'contain',
    backgroundColor: 'black',
    justifyContent: 'space-between',
    height: height / 17,
  },
  approva: {
    backgroundColor: '#0a0a0a',
    width: width / 2.2,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  testo: {
    color: 'white',
    fontFamily: 'verdana',
  },
  nota: {
    color: 'green',
    fontFamily: 'verdana',
    marginTop: 10,
    marginLeft: 7,
  },
  imagecomunication: {
    height: height,
    width: width,
    resizeMode: 'contain',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewSearch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: height / 18,
    backgroundColor: 'black',
    width: width,
  },
  state: {
    flexDirection: 'row',
    marginRight: 10,
  },
  textInput1: {
    fontFamily: 'verdana',
    marginHorizontal: 10,
    fontSize: 18,
    marginTop: 5,
    color: 'green',
    fontWeight: 'bold',
  },

  textInput: {
    fontFamily: 'verdana',
    marginHorizontal: 10,
    fontSize: 19,
    marginTop: 5,

    color: 'white',
  },
  description: {
    color: 'white',
  },
  close: {
    backgroundColor: 'red',
    width: 30,
    height: 30,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 2,
  },
});
