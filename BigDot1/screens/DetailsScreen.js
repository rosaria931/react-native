/* eslint-disable prettier/prettier */
import React from 'react';
import {Button, View, Text, StyleSheet} from 'react-native';

const DetailsScreen = ({navigation}) => {
    return (
      <View style={styles.container}>
        <Text> Details Screen</Text>
        <Button
          title="Go to details screen...again"
          onPress={() => navigation.push('Details')}
        />
        <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
        <Button title="Go back" onPress={() => navigation.goBack()} />
        <Button
          title="Go to the first screen"
          onPress={() => navigation.popToTop()}
        />
      </View>
    );
  };

  export default DetailsScreen;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
