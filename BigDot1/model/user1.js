/* eslint-disable prettier/prettier */
export default (Users1 = [
  {username: 'user1', password: 'password', level: 1, role: 'Admin'},
  {username: 'rosaria', password: 'rosaria1234', level: 1, role: 'Admin'},
  {username: 'gbellizzi', password: 'gbellizzi', level: 1, role: 'Admin'},
  {username: 'aleo', password: 'aleo1234', level: 2, role: 'Admin'},
  {username: 'famatruda', password: 'famatruda1234', level: 2, role: 'Admin'},
  {
    username: 'lgagliardi',
    password: 'lgagliardi1234',
    level: 3,
    role: 'Admin',
  },
  {
    username: 'ggusmeroli',
    password: 'ggusmeroli1234',
    level: 2,
    role: 'Approver',
  },
  {username: 'spizzuto', password: 'spizzuto1234', level: 3, role: 'Approver'},
  {username: 'amercuri', password: 'amercuri1234', level: 4, role: 'Approver'},
  {
    username: 'mdibuono',
    password: 'mdibuono1234',
    level: 5,
    role: 'Pratictioner',
  },
  {
    username: 'aesposito',
    password: 'aesposito1234',
    level: 5,
    role: 'Pratictioner',
  },
  {
    username: 'tdigiacinto',
    password: 'tdigiacinto1234',
    level: 5,
    role: 'Pratictioner',
  },
  {
    username: 'lvitale',
    password: 'lvitale1234',
    level: 5,
    role: 'Pratictioner',
  },
]);
