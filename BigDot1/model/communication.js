export class Communication {
  title:String,
  date:String,
  imgUrl:String,
  pdfUrl: String,
  body:String,
  categorie:Number
}
