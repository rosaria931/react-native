/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {DrawerContent} from './screens/DrawerContent';
import DeniedPreviewScreen from './screens/DeniedPreviewScreen';
import DeniedStatusScreen from './screens/DeniedStatusScreen';

import ComunicationScreen2 from './screens/ComunicationScreen2';
import PreviewScreen from './screens/PreviewScreen';
import StatusScreen2 from './screens/StatusScreen2';

import ApprovedPreviewScreen from './screens/ApprovedPreviewScreen';
import ApproverStatusScreen from './screens/ApproverStatusScreen';

import PendingPreviewScreen from './screens/PendingPreviewScreen';
import PendingStatusScreen from './screens/PendingStatusScreen';
import PendingScreen2 from './screens/PendingScreen2';

import {AuthContext} from './components/context';

import PurposeScreen from './screens/PurposeScreen';
import ConsultingLeaderScreen from './screens/ConsultingLeaderScreen';
import DiversityScreen from './screens/DiversityScreen';
import WBScreen from './screens/WBScreen';
import EventiScreen from './screens/EventiScreen';
import TalentScreen from './screens/TalentScreen';
import POScreen from './screens/POScreen';

import PratictionerTabScreen from './screens/PratictionerTabScreen';
import ApproverTabScreen from './screens/ApproverTabScreen';
import RootStackScreen from './screens/RootStackScreen';

import AsyncStorage from '@react-native-community/async-storage';

import HeaderLogo from './components/headerLogo';

const Drawer = createDrawerNavigator();

const App = () => {
  // const [isLoading, setIsLoading] = React.useState(true);
  // const [userToken, setUserToken] = React.useState(null);

  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToken: null,
  };

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userName: null,
          userToken: null,
          isLoading: false,
        };
      case 'REGISTER':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
    }
  };

  const [loginState, dispatch] = React.useReducer(
    loginReducer,
    initialLoginState,
  );

  const authContext = React.useMemo(
    () => ({
      signIn: async foundUser => {
        // setUserToken('fgkj');
        // setIsLoading(false);
        const userToken = String(foundUser[0].userToken);
        const userName = foundUser[0].username;

        try {
          await AsyncStorage.setItem('userToken', userToken);
        } catch (e) {
          console.log(e);
        }
        // console.log('user token: ', userToken);
        dispatch({type: 'LOGIN', id: userName, token: userToken});
      },
      signOut: async () => {
        // setUserToken(null);
        // setIsLoading(false);
        try {
          await AsyncStorage.removeItem('userToken');
        } catch (e) {
          console.log(e);
        }
        dispatch({type: 'LOGOUT'});
      },
      signUp: () => {
        // setUserToken('fgkj');
        // setIsLoading(false);
      },
    }),
    [],
  );

  useEffect(() => {
    setTimeout(async () => {
      // setIsLoading(false);
      let userToken;
      userToken = null;
      try {
        userToken = await AsyncStorage.getItem('userToken');
      } catch (e) {
        console.log(e);
      }
      // console.log('user token: ', userToken);
      dispatch({type: 'RETRIEVE_TOKEN', token: userToken});
    }, 1000);
  }, []);

  if (loginState.isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" />
      </View>
    );
  }
  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <HeaderLogo />
        {loginState.userToken !== null ? (
          <Drawer.Navigator
            drawerPosition={'left'}
            drawerContent={props => <DrawerContent {...props} />}>
            <Drawer.Screen name="Home" component={PratictionerTabScreen} />
            <Drawer.Screen name="Approver" component={ApproverTabScreen} />
            <Drawer.Screen
              name="ComunicationScreen2"
              component={ComunicationScreen2}
            />
            <Drawer.Screen name="PreviewScreen" component={PreviewScreen} />
            <Drawer.Screen name="StatusScreen2" component={StatusScreen2} />
            <Drawer.Screen name="PendingScreen2" component={PendingScreen2} />
            <Drawer.Screen name="PurposeScreen" component={PurposeScreen} />
            <Drawer.Screen
              name="ConsultingLeaderScreen"
              component={ConsultingLeaderScreen}
            />
            <Drawer.Screen name="DiversityScreen" component={DiversityScreen} />
            <Drawer.Screen name="WBScreen" component={WBScreen} />
            <Drawer.Screen name="EventiScreen" component={EventiScreen} />
            <Drawer.Screen name="TalentScreen" component={TalentScreen} />
            <Drawer.Screen name="POScreen" component={POScreen} />
            <Drawer.Screen
              name="DeniedPreviewScreen"
              component={DeniedPreviewScreen}
            />
            <Drawer.Screen
              name="ApprovedPreviewScreen"
              component={ApprovedPreviewScreen}
            />
            <Drawer.Screen
              name="PendingPreviewScreen"
              component={PendingPreviewScreen}
            />
            <Drawer.Screen
              name="DeniedStatusScreen"
              component={DeniedStatusScreen}
            />
            <Drawer.Screen
              name="ApproverStatusScreen"
              component={ApproverStatusScreen}
            />
            <Drawer.Screen
              name="PendingStatusScreen"
              component={PendingStatusScreen}
            />
          </Drawer.Navigator>
        ) : (
          <RootStackScreen />
        )}
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;
